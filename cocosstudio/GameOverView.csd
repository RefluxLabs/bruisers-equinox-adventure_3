<GameFile>
  <PropertyGroup Name="GameOverView" Type="Layer" ID="73edf1c2-6e68-4b48-bc9d-7ddbd5a7785b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="60" Speed="1.0000">
        <Timeline ActionTag="301840654" Property="Position">
          <PointFrame FrameIndex="0" X="959.9984" Y="532.2794">
            <EasingData Type="1" />
          </PointFrame>
          <PointFrame FrameIndex="60" X="959.9984" Y="532.2794">
            <EasingData Type="3" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="301840654" Property="Scale">
          <ScaleFrame FrameIndex="0" X="9.6835" Y="5.4646">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="301840654" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="GameOverViewLayer" UserData="OHH YeahDuff MAn" Tag="106" ctype="GameLayerObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Sprite_bg" ActionTag="491194757" Tag="188" IconVisible="False" LeftMargin="-4.0043" RightMargin="-1943.9956" TopMargin="0.0327" BottomMargin="-0.0327" ctype="SpriteObjectData">
            <Size X="3868.0000" Y="1080.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1929.9957" Y="539.9673" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.0052" Y="0.5000" />
            <PreSize X="2.0146" Y="1.0000" />
            <FileData Type="Normal" Path="environment1/BG_1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="PageView_1" ActionTag="301840654" CallBackType="Event" Tag="107" IconVisible="False" LeftMargin="860.5784" RightMargin="859.4216" TopMargin="446.1206" BottomMargin="433.8794" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" ctype="PageViewObjectData">
            <Size X="200.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="Panel_1" ActionTag="-1124703722" Alpha="204" CallBackType="Click" Tag="108" IconVisible="False" RightMargin="600.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="75" ComboBoxIndex="2" ColorAngle="79.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="200.0000" Y="200.0000" />
                <Children>
                  <AbstractNodeData Name="ScrollView_1" ActionTag="-1226471829" Tag="218" IconVisible="False" LeftMargin="32.5846" RightMargin="-32.5846" TopMargin="-6.1375" BottomMargin="6.1375" TouchEnable="True" ClipAble="False" BackColorAlpha="25" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
                    <Size X="200.0000" Y="200.0000" />
                    <Children>
                      <AbstractNodeData Name="title_Goals" ActionTag="949725948" Tag="219" IconVisible="False" LeftMargin="70.2468" RightMargin="76.7532" TopMargin="5.9905" BottomMargin="244.0095" FontSize="20" LabelText="Goals&#xA;" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="53.0000" Y="50.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="96.7468" Y="269.0095" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="26" G="26" B="26" />
                        <PrePosition X="0.4837" Y="0.8967" />
                        <PreSize X="0.2650" Y="0.1667" />
                        <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="ListView_1" ActionTag="29251493" Tag="220" IconVisible="False" LeftMargin="1.8628" RightMargin="-1.8628" TopMargin="28.0470" BottomMargin="71.9530" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" DirectionType="Vertical" ctype="ListViewObjectData">
                        <Size X="200.0000" Y="200.0000" />
                        <Children>
                          <AbstractNodeData Name="Text_8_1" ActionTag="1184171317" Tag="222" IconVisible="False" RightMargin="62.0000" BottomMargin="170.0000" FontSize="12" LabelText="Travel 500m in one run!&#xA;" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                            <Size X="138.0000" Y="30.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="69.0000" Y="185.0000" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="0" G="0" B="0" />
                            <PrePosition X="0.3450" Y="0.9250" />
                            <PreSize X="0.6900" Y="0.1500" />
                            <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                            <OutlineColor A="255" R="255" G="0" B="0" />
                            <ShadowColor A="255" R="110" G="110" B="110" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="loadingBar" ActionTag="1879512770" ZOrder="1" CallBackType="Touch" CallBackName="loadingBar_" UserData="datadata" Tag="134" IconVisible="False" RightMargin="20.0000" TopMargin="30.0000" BottomMargin="156.0000" TouchEnable="True" ProgressInfo="20" ctype="LoadingBarObjectData">
                            <Size X="180.0000" Y="14.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="90.0000" Y="163.0000" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.4500" Y="0.8150" />
                            <PreSize X="0.9000" Y="0.0700" />
                            <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint />
                        <Position X="1.8628" Y="71.9530" />
                        <Scale ScaleX="1.0000" ScaleY="0.9369" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.0093" Y="0.2398" />
                        <PreSize X="1.0000" Y="0.6667" />
                        <SingleColor A="255" R="127" G="127" B="127" />
                        <FirstColor A="255" R="150" G="150" B="255" />
                        <EndColor A="255" R="255" G="255" B="255" />
                        <ColorVector ScaleY="1.0000" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position X="32.5846" Y="6.1375" />
                    <Scale ScaleX="0.7235" ScaleY="0.9362" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1629" Y="0.0307" />
                    <PreSize X="1.0000" Y="1.0000" />
                    <SingleColor A="255" R="255" G="150" B="100" />
                    <FirstColor A="255" R="255" G="150" B="100" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                    <InnerNodeSize Width="200" Height="300" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.2500" Y="1.0000" />
                <SingleColor A="255" R="167" G="190" B="215" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleX="0.1908" ScaleY="0.9816" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_2" ActionTag="-1612952657" ZOrder="1" Tag="109" IconVisible="False" LeftMargin="200.0000" RightMargin="400.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="76" ComboBoxIndex="2" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="200.0000" Y="200.0000" />
                <Children>
                  <AbstractNodeData Name="Panel_2.1" ActionTag="122236699" Alpha="204" Tag="118" IconVisible="False" LeftMargin="2.0648" RightMargin="-2.0648" TopMargin="3.6597" BottomMargin="-3.6597" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="200.0000" Y="200.0000" />
                    <Children>
                      <AbstractNodeData Name="ScrollView_1" ActionTag="-843996971" Tag="119" IconVisible="False" LeftMargin="32.5846" RightMargin="-32.5846" TopMargin="-6.1375" BottomMargin="6.1375" TouchEnable="True" ClipAble="False" BackColorAlpha="50" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
                        <Size X="200.0000" Y="200.0000" />
                        <Children>
                          <AbstractNodeData Name="Text_1" ActionTag="-1918713785" Tag="120" IconVisible="False" LeftMargin="73.7468" RightMargin="80.2532" TopMargin="5.9905" BottomMargin="244.0095" FontSize="20" LabelText="Stats&#xA;" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                            <Size X="46.0000" Y="50.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="96.7468" Y="269.0095" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="26" G="26" B="26" />
                            <PrePosition X="0.4837" Y="0.8967" />
                            <PreSize X="0.2300" Y="0.1667" />
                            <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                            <OutlineColor A="255" R="255" G="0" B="0" />
                            <ShadowColor A="255" R="110" G="110" B="110" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="ListView_1" ActionTag="560323841" Alpha="228" Tag="122" IconVisible="False" LeftMargin="1.8628" RightMargin="-1.8628" TopMargin="28.0470" BottomMargin="71.9530" TouchEnable="True" ClipAble="False" BackColorAlpha="178" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="0" ItemMargin="5" DirectionType="Vertical" ctype="ListViewObjectData">
                            <Size X="200.0000" Y="200.0000" />
                            <Children>
                              <AbstractNodeData Name="labelScore" ActionTag="-362181506" Tag="126" IconVisible="False" RightMargin="40.0000" BottomMargin="269.0000" TouchEnable="True" IsCustomSize="True" FontSize="14" LabelText="this.labelScore-&gt;this.score" TouchScaleChangeAble="True" OutlineSize="25" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                                <Size X="160.0000" Y="20.0000" />
                                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                                <Position X="80.0000" Y="279.0000" />
                                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                                <CColor A="255" R="0" G="0" B="0" />
                                <PrePosition X="0.4000" Y="0.9654" />
                                <PreSize X="0.8000" Y="0.0692" />
                                <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                                <OutlineColor A="255" R="0" G="0" B="0" />
                                <ShadowColor A="255" R="110" G="110" B="110" />
                              </AbstractNodeData>
                              <AbstractNodeData Name="HighScore" ActionTag="-259294006" ZOrder="1" Tag="127" IconVisible="False" RightMargin="40.0000" TopMargin="25.0000" BottomMargin="244.0000" IsCustomSize="True" FontSize="14" LabelText="this.highscore-&gt;this.highscore&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                                <Size X="160.0000" Y="20.0000" />
                                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                                <Position X="80.0000" Y="254.0000" />
                                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                                <CColor A="255" R="0" G="0" B="0" />
                                <PrePosition X="0.4000" Y="0.8789" />
                                <PreSize X="0.8000" Y="0.0692" />
                                <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                                <OutlineColor A="255" R="255" G="0" B="0" />
                                <ShadowColor A="255" R="110" G="110" B="110" />
                              </AbstractNodeData>
                              <AbstractNodeData Name="labelCoin" ActionTag="-1984685108" ZOrder="2" CallBackType="Touch" Tag="128" IconVisible="False" RightMargin="40.0000" TopMargin="50.0000" BottomMargin="219.0000" IsCustomSize="True" FontSize="14" LabelText="this.labelCoin-&gt;this.cval&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                                <Size X="160.0000" Y="20.0000" />
                                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                                <Position X="80.0000" Y="229.0000" />
                                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                                <CColor A="255" R="0" G="0" B="0" />
                                <PrePosition X="0.4000" Y="0.7924" />
                                <PreSize X="0.8000" Y="0.0692" />
                                <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                                <OutlineColor A="255" R="255" G="0" B="0" />
                                <ShadowColor A="255" R="110" G="110" B="110" />
                              </AbstractNodeData>
                              <AbstractNodeData Name="labelBestCoin" ActionTag="-1300177176" ZOrder="3" Tag="129" IconVisible="False" RightMargin="40.0000" TopMargin="75.0000" BottomMargin="194.0000" IsCustomSize="True" FontSize="14" LabelText="this.labelBestCoin-&gt;this.bestCoinNum&#xA;&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                                <Size X="160.0000" Y="20.0000" />
                                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                                <Position X="80.0000" Y="204.0000" />
                                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                                <CColor A="255" R="0" G="0" B="0" />
                                <PrePosition X="0.4000" Y="0.7059" />
                                <PreSize X="0.8000" Y="0.0692" />
                                <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                                <OutlineColor A="255" R="255" G="0" B="0" />
                                <ShadowColor A="255" R="110" G="110" B="110" />
                              </AbstractNodeData>
                              <AbstractNodeData Name="labelTotalCoin" ActionTag="882266874" ZOrder="4" Tag="130" IconVisible="False" RightMargin="40.0000" TopMargin="100.0000" BottomMargin="169.0000" IsCustomSize="True" FontSize="14" LabelText="this.labelTotalCoin-&gt;this.totalCoin" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                                <Size X="160.0000" Y="20.0000" />
                                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                                <Position X="80.0000" Y="179.0000" />
                                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                                <CColor A="255" R="0" G="0" B="0" />
                                <PrePosition X="0.4000" Y="0.6194" />
                                <PreSize X="0.8000" Y="0.0692" />
                                <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                                <OutlineColor A="255" R="255" G="0" B="0" />
                                <ShadowColor A="255" R="110" G="110" B="110" />
                              </AbstractNodeData>
                              <AbstractNodeData Name="labelDistance" ActionTag="-65727338" ZOrder="5" Tag="131" IconVisible="False" RightMargin="13.0000" TopMargin="125.0000" BottomMargin="146.0000" FontSize="14" LabelText="this.labelDistance-&gt; this.mval" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                                <Size X="187.0000" Y="18.0000" />
                                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                                <Position X="93.5000" Y="155.0000" />
                                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                                <CColor A="255" R="0" G="0" B="0" />
                                <PrePosition X="0.4675" Y="0.5363" />
                                <PreSize X="0.9350" Y="0.0623" />
                                <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                                <OutlineColor A="255" R="255" G="0" B="0" />
                                <ShadowColor A="255" R="110" G="110" B="110" />
                              </AbstractNodeData>
                              <AbstractNodeData Name="labelBestDistance" ActionTag="1981101714" ZOrder="6" Tag="132" IconVisible="False" RightMargin="40.0000" TopMargin="148.0000" BottomMargin="121.0000" IsCustomSize="True" FontSize="14" LabelText="this.labelBestDistance-&gt;this.mval" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                                <Size X="160.0000" Y="20.0000" />
                                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                                <Position X="80.0000" Y="131.0000" />
                                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                                <CColor A="255" R="0" G="0" B="0" />
                                <PrePosition X="0.4000" Y="0.4533" />
                                <PreSize X="0.8000" Y="0.0692" />
                                <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                                <OutlineColor A="255" R="255" G="0" B="0" />
                                <ShadowColor A="255" R="110" G="110" B="110" />
                              </AbstractNodeData>
                              <AbstractNodeData Name="labelTotalDistance" ActionTag="1945123235" ZOrder="7" Tag="133" IconVisible="False" RightMargin="40.0000" TopMargin="173.0000" BottomMargin="96.0000" IsCustomSize="True" FontSize="14" LabelText="this.labelTotalDistance-&gt;this.totalDistanceNum" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                                <Size X="160.0000" Y="20.0000" />
                                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                                <Position X="80.0000" Y="106.0000" />
                                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                                <CColor A="255" R="0" G="0" B="0" />
                                <PrePosition X="0.4000" Y="0.3668" />
                                <PreSize X="0.8000" Y="0.0692" />
                                <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                                <OutlineColor A="255" R="255" G="0" B="0" />
                                <ShadowColor A="255" R="110" G="110" B="110" />
                              </AbstractNodeData>
                              <AbstractNodeData Name="labelLevel" ActionTag="1549101220" ZOrder="8" Tag="134" IconVisible="False" RightMargin="3.0000" TopMargin="198.0000" BottomMargin="73.0000" FontSize="14" LabelText="this.labelLevel-&gt; this.levelNum" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                                <Size X="197.0000" Y="18.0000" />
                                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                                <Position X="98.5000" Y="82.0000" />
                                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                                <CColor A="255" R="0" G="0" B="0" />
                                <PrePosition X="0.4925" Y="0.2837" />
                                <PreSize X="0.9850" Y="0.0623" />
                                <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                                <OutlineColor A="255" R="255" G="0" B="0" />
                                <ShadowColor A="255" R="110" G="110" B="110" />
                              </AbstractNodeData>
                              <AbstractNodeData Name="labelLivesRemaining" ActionTag="1418599940" ZOrder="9" Tag="135" IconVisible="False" RightMargin="12.0000" TopMargin="221.0000" BottomMargin="50.0000" FontSize="14" LabelText="this.labelLevel-&gt; this.lifeNum" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                                <Size X="188.0000" Y="18.0000" />
                                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                                <Position X="94.0000" Y="59.0000" />
                                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                                <CColor A="255" R="0" G="0" B="0" />
                                <PrePosition X="0.4700" Y="0.2042" />
                                <PreSize X="0.9400" Y="0.0623" />
                                <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                                <OutlineColor A="255" R="255" G="0" B="0" />
                                <ShadowColor A="255" R="110" G="110" B="110" />
                              </AbstractNodeData>
                              <AbstractNodeData Name="labelTotalDeaths" ActionTag="90088006" ZOrder="10" Tag="313" IconVisible="False" RightMargin="40.0000" TopMargin="244.0000" BottomMargin="25.0000" IsCustomSize="True" FontSize="14" LabelText="this.labelTotalDeaths-&gt;this.totalDeathNum" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                                <Size X="160.0000" Y="20.0000" />
                                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                                <Position X="80.0000" Y="35.0000" />
                                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                                <CColor A="255" R="0" G="0" B="0" />
                                <PrePosition X="0.4000" Y="0.1211" />
                                <PreSize X="0.8000" Y="0.0692" />
                                <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                                <OutlineColor A="255" R="255" G="0" B="0" />
                                <ShadowColor A="255" R="110" G="110" B="110" />
                              </AbstractNodeData>
                              <AbstractNodeData Name="labelTotalStars" ActionTag="-74676742" ZOrder="11" CallBackType="Touch" CallBackName="starsMethod" UserData="totalstars" Tag="137" FrameEvent="starsEvent" IconVisible="False" RightMargin="40.0000" TopMargin="269.0000" IsCustomSize="True" FontSize="14" LabelText="this.labelTotalStars-&gt; this.stars" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                                <Size X="160.0000" Y="20.0000" />
                                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                                <Position X="80.0000" Y="10.0000" />
                                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                                <CColor A="255" R="0" G="0" B="0" />
                                <PrePosition X="0.4000" Y="0.0346" />
                                <PreSize X="0.8000" Y="0.0692" />
                                <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                                <OutlineColor A="255" R="255" G="0" B="0" />
                                <ShadowColor A="255" R="110" G="110" B="110" />
                              </AbstractNodeData>
                            </Children>
                            <AnchorPoint />
                            <Position X="1.8628" Y="71.9530" />
                            <Scale ScaleX="1.0000" ScaleY="0.9369" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.0093" Y="0.2398" />
                            <PreSize X="1.0000" Y="0.6667" />
                            <SingleColor A="255" R="127" G="127" B="127" />
                            <FirstColor A="255" R="150" G="150" B="255" />
                            <EndColor A="255" R="255" G="255" B="255" />
                            <ColorVector ScaleY="1.0000" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint />
                        <Position X="32.5846" Y="6.1375" />
                        <Scale ScaleX="0.7235" ScaleY="0.9362" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1629" Y="0.0307" />
                        <PreSize X="1.0000" Y="1.0000" />
                        <SingleColor A="255" R="255" G="150" B="100" />
                        <FirstColor A="255" R="255" G="150" B="100" />
                        <EndColor A="255" R="255" G="255" B="255" />
                        <ColorVector ScaleY="1.0000" />
                        <InnerNodeSize Width="200" Height="300" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position X="2.0648" Y="-3.6597" />
                    <Scale ScaleX="0.9897" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0103" Y="-0.0183" />
                    <PreSize X="1.0000" Y="1.0000" />
                    <SingleColor A="255" R="167" G="190" B="215" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="200.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2500" />
                <PreSize X="0.2500" Y="1.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_3" ActionTag="1188127861" ZOrder="2" Tag="124" IconVisible="False" LeftMargin="400.0000" RightMargin="200.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="200.0000" Y="200.0000" />
                <Children>
                  <AbstractNodeData Name="Panel_3_1" ActionTag="-1491598709" Tag="171" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="200.0000" Y="200.0000" />
                    <Children>
                      <AbstractNodeData Name="ScrollView_1" ActionTag="1735175896" Tag="125" IconVisible="False" LeftMargin="32.5846" RightMargin="-32.5846" TopMargin="-6.1375" BottomMargin="6.1375" TouchEnable="True" ClipAble="False" BackColorAlpha="76" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
                        <Size X="200.0000" Y="200.0000" />
                        <Children>
                          <AbstractNodeData Name="Text_1" ActionTag="244607251" Tag="126" IconVisible="False" LeftMargin="33.9057" RightMargin="31.0943" TopMargin="3.5887" BottomMargin="221.4113" FontSize="20" LabelText="Google Play &#xA;Achievements!&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                            <Size X="135.0000" Y="75.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="101.4057" Y="258.9113" />
                            <Scale ScaleX="1.0000" ScaleY="0.9327" />
                            <CColor A="255" R="26" G="26" B="26" />
                            <PrePosition X="0.5070" Y="0.8630" />
                            <PreSize X="0.6750" Y="0.2500" />
                            <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                            <OutlineColor A="255" R="255" G="0" B="0" />
                            <ShadowColor A="255" R="110" G="110" B="110" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="CheckBox_1" ActionTag="-2023299716" Tag="136" IconVisible="False" LeftMargin="90.4952" RightMargin="69.5048" TopMargin="245.2311" BottomMargin="14.7689" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
                            <Size X="40.0000" Y="40.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="110.4952" Y="34.7689" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.5525" Y="0.1159" />
                            <PreSize X="0.2000" Y="0.1333" />
                            <NormalBackFileData Type="Default" Path="Default/CheckBox_Normal.png" Plist="" />
                            <NodeNormalFileData Type="Default" Path="Default/CheckBoxNode_Normal.png" Plist="" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint />
                        <Position X="32.5846" Y="6.1375" />
                        <Scale ScaleX="0.7235" ScaleY="0.9362" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1629" Y="0.0307" />
                        <PreSize X="1.0000" Y="1.0000" />
                        <SingleColor A="255" R="127" G="127" B="127" />
                        <FirstColor A="255" R="255" G="150" B="100" />
                        <EndColor A="255" R="255" G="255" B="255" />
                        <ColorVector ScaleY="1.0000" />
                        <InnerNodeSize Width="200" Height="300" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position />
                    <Scale ScaleX="0.9897" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="1.0000" Y="1.0000" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="400.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" />
                <PreSize X="0.2500" Y="1.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_4" ActionTag="1183969127" ZOrder="3" Alpha="204" Tag="254" IconVisible="False" LeftMargin="600.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="75" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="200.0000" Y="200.0000" />
                <Children>
                  <AbstractNodeData Name="Text_1" ActionTag="-1192445163" Tag="256" IconVisible="False" LeftMargin="69.6066" RightMargin="53.3934" TopMargin="10.1280" BottomMargin="135.8720" FontSize="20" LabelText="Settings&#xA;" HorizontalAlignmentType="HT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="77.0000" Y="54.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="108.1066" Y="162.8720" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="26" G="26" B="26" />
                    <PrePosition X="0.5405" Y="0.8144" />
                    <PreSize X="0.3850" Y="0.2700" />
                    <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                    <OutlineColor A="255" R="26" G="26" B="26" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Panel_4.1" ActionTag="-1058758347" Tag="316" IconVisible="False" LeftMargin="46.2365" RightMargin="-46.2365" TopMargin="-27.5083" BottomMargin="27.5083" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="200.0000" Y="200.0000" />
                    <Children>
                      <AbstractNodeData Name="label_sound" ActionTag="-1814416460" Tag="317" IconVisible="False" LeftMargin="13.7120" RightMargin="136.2880" TopMargin="38.8581" BottomMargin="125.1419" FontSize="14" LabelText="Sounds &#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="50.0000" Y="36.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="38.7120" Y="143.1419" />
                        <Scale ScaleX="1.1442" ScaleY="1.4738" />
                        <CColor A="255" R="26" G="26" B="26" />
                        <PrePosition X="0.1936" Y="0.7157" />
                        <PreSize X="0.2500" Y="0.1800" />
                        <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="label_music" ActionTag="1792841045" Tag="318" IconVisible="False" LeftMargin="12.5791" RightMargin="146.4209" TopMargin="95.4201" BottomMargin="68.5799" FontSize="14" LabelText="Music&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="41.0000" Y="36.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="33.0791" Y="86.5799" />
                        <Scale ScaleX="1.1442" ScaleY="1.4738" />
                        <CColor A="255" R="26" G="26" B="26" />
                        <PrePosition X="0.1654" Y="0.4329" />
                        <PreSize X="0.2050" Y="0.1800" />
                        <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="label_difficulty" ActionTag="89338014" Tag="319" IconVisible="False" LeftMargin="14.3395" RightMargin="122.6605" TopMargin="158.9366" BottomMargin="5.0634" FontSize="14" LabelText="Difficulty&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="63.0000" Y="36.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="45.8395" Y="23.0634" />
                        <Scale ScaleX="1.1442" ScaleY="1.4738" />
                        <CColor A="255" R="26" G="26" B="26" />
                        <PrePosition X="0.2292" Y="0.1153" />
                        <PreSize X="0.3150" Y="0.1800" />
                        <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Slider_sounds" ActionTag="-473415993" VisibleForFrame="False" Tag="321" IconVisible="False" LeftMargin="-0.5234" RightMargin="-73.4766" TopMargin="43.0022" BottomMargin="138.9978" TouchEnable="True" PercentInfo="50" ctype="SliderObjectData">
                        <Size X="274.0000" Y="18.0000" />
                        <AnchorPoint ScaleX="0.4732" ScaleY="0.3570" />
                        <Position X="129.1334" Y="145.4238" />
                        <Scale ScaleX="0.4260" ScaleY="2.1599" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.6457" Y="0.7271" />
                        <PreSize X="1.3700" Y="0.0900" />
                        <BackGroundData Type="Normal" Path="Setting/DnE10.png" Plist="" />
                        <ProgressBarData Type="Normal" Path="Setting/DnE15.png" Plist="" />
                        <BallPressedData Type="Default" Path="Default/SliderNode_Press.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Slider_sounds" ActionTag="367948310" Tag="320" IconVisible="False" LeftMargin="21.3099" RightMargin="-21.3099" TopMargin="105.3370" BottomMargin="80.6630" TouchEnable="True" PercentInfo="50" ctype="SliderObjectData">
                        <Size X="200.0000" Y="14.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="121.3099" Y="87.6630" />
                        <Scale ScaleX="0.3711" ScaleY="1.3779" />
                        <CColor A="255" R="26" G="26" B="26" />
                        <PrePosition X="0.6065" Y="0.4383" />
                        <PreSize X="1.0000" Y="0.0700" />
                        <BackGroundData Type="Default" Path="Default/Slider_Back.png" Plist="" />
                        <ProgressBarData Type="Default" Path="Default/Slider_PressBar.png" Plist="" />
                        <BallNormalData Type="Default" Path="Default/SliderNode_Normal.png" Plist="" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position X="46.2365" Y="27.5083" />
                    <Scale ScaleX="0.5869" ScaleY="0.5791" />
                    <CColor A="255" R="167" G="190" B="215" />
                    <PrePosition X="0.2312" Y="0.1375" />
                    <PreSize X="1.0000" Y="1.0000" />
                    <SingleColor A="255" R="167" G="190" B="215" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="600.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7500" />
                <PreSize X="0.2500" Y="1.0000" />
                <SingleColor A="255" R="167" G="190" B="215" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.4971" ScaleY="0.4920" />
            <Position X="959.9984" Y="532.2794" />
            <Scale ScaleX="9.6835" ScaleY="5.4646" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4929" />
            <PreSize X="0.1042" Y="0.1852" />
            <SingleColor A="255" R="98" G="177" B="208" />
            <FirstColor A="255" R="150" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_back" ActionTag="1207958050" CallBackType="Touch" CallBackName="btn_back" UserData="btn_back touched" Tag="38" IconVisible="False" LeftMargin="90.7287" RightMargin="1706.2712" TopMargin="880.4742" BottomMargin="76.5258" TouchEnable="True" FontSize="14" ButtonText="Back" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="93" Scale9Height="101" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="123.0000" Y="123.0000" />
            <AnchorPoint ScaleX="0.4590" ScaleY="0.5000" />
            <Position X="147.1857" Y="138.0258" />
            <Scale ScaleX="1.2963" ScaleY="1.1896" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0767" Y="0.1278" />
            <PreSize X="0.0641" Y="0.1139" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="btn/button_artbord-03.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>