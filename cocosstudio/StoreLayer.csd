<GameFile>
  <PropertyGroup Name="StoreLayer" Type="Layer" ID="8fb5d223-d567-42fc-b039-e0de5db2b2e8" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" UserData="StoreLayer Panel" Tag="39" ctype="GameLayerObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Panel_2" ActionTag="162117614" UserData="Panel_2 in store layer" Tag="44" IconVisible="False" RightMargin="1720.0000" TopMargin="880.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="200.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="ScrollView_1" ActionTag="754346157" UserData="ScrollView_1 ----Hacked" Tag="45" IconVisible="False" LeftMargin="37.4246" RightMargin="-37.4246" TopMargin="283.1583" BottomMargin="-283.1583" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
                <Size X="200.0000" Y="200.0000" />
                <Children>
                  <AbstractNodeData Name="Panel_1" ActionTag="-1163324240" UserData="Panel_1 --Hacked" Tag="48" IconVisible="False" LeftMargin="2.6653" RightMargin="-2.6653" TopMargin="32.7666" BottomMargin="67.2334" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="200.0000" Y="200.0000" />
                    <Children>
                      <AbstractNodeData Name="Store_Board" ActionTag="954351264" UserData="storeboard.png" Tag="43" IconVisible="False" LeftMargin="-194.4571" RightMargin="-193.5429" TopMargin="-309.1251" BottomMargin="-255.8749" ctype="SpriteObjectData">
                        <Size X="588.0000" Y="765.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="99.5429" Y="126.6251" />
                        <Scale ScaleX="0.3373" ScaleY="0.1926" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.4977" Y="0.6331" />
                        <PreSize X="2.9400" Y="3.8250" />
                        <FileData Type="Normal" Path="store-board.png" Plist="" />
                        <BlendFunc Src="770" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="btn_buymagnet" ActionTag="1141920607" Tag="49" IconVisible="False" LeftMargin="80.0579" RightMargin="-10.0579" TopMargin="28.0848" BottomMargin="127.9152" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="22" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                        <Size X="130.0000" Y="44.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="145.0579" Y="149.9152" />
                        <Scale ScaleX="0.3583" ScaleY="0.2099" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.7253" Y="0.7496" />
                        <PreSize X="0.6500" Y="0.2200" />
                        <TextColor A="255" R="65" G="65" B="70" />
                        <NormalFileData Type="Normal" Path="buy-50.png" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="btn_buyshoes" ActionTag="-791467478" UserData="btn_buyshoes_purple" Tag="66" IconVisible="False" LeftMargin="79.5956" RightMargin="-9.5956" TopMargin="54.6998" BottomMargin="101.3002" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="22" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                        <Size X="130.0000" Y="44.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="144.5956" Y="123.3002" />
                        <Scale ScaleX="0.3583" ScaleY="0.2099" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.7230" Y="0.6165" />
                        <PreSize X="0.6500" Y="0.2200" />
                        <TextColor A="255" R="65" G="65" B="70" />
                        <NormalFileData Type="Normal" Path="buy-50.png" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="btn_buyredshoes" ActionTag="-338622401" UserData="btn_buyredshoes" Tag="67" IconVisible="False" LeftMargin="80.7220" RightMargin="-10.7220" TopMargin="80.9252" BottomMargin="75.0748" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="22" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                        <Size X="130.0000" Y="44.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="145.7220" Y="97.0748" />
                        <Scale ScaleX="0.3583" ScaleY="0.2099" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.7286" Y="0.4854" />
                        <PreSize X="0.6500" Y="0.2200" />
                        <TextColor A="255" R="65" G="65" B="70" />
                        <NormalFileData Type="Normal" Path="buy-50.png" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="txt_magnet" ActionTag="1625825666" CallBackType="Touch" UserData="txt_magnet" Tag="70" IconVisible="False" LeftMargin="52.8091" RightMargin="139.1909" TopMargin="116.0506" BottomMargin="51.9494" FontSize="14" LabelText="0&#xA;" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="8.0000" Y="32.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="56.8091" Y="67.9494" />
                        <Scale ScaleX="2.4655" ScaleY="0.3472" />
                        <CColor A="255" R="0" G="0" B="0" />
                        <PrePosition X="0.2840" Y="0.3397" />
                        <PreSize X="0.0400" Y="0.1600" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="txt_shoes" ActionTag="1412499347" CallBackType="Touch" UserData="txt_purpleshoes" Tag="73" FrameEvent="btn_buyshoesEvent" IconVisible="False" LeftMargin="107.9271" RightMargin="84.0729" TopMargin="111.8787" BottomMargin="56.1213" FontSize="14" LabelText="0&#xA;" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="8.0000" Y="32.0000" />
                        <AnchorPoint ScaleX="0.5904" ScaleY="0.3330" />
                        <Position X="112.6503" Y="66.7773" />
                        <Scale ScaleX="2.3069" ScaleY="0.3079" />
                        <CColor A="255" R="0" G="0" B="0" />
                        <PrePosition X="0.5633" Y="0.3339" />
                        <PreSize X="0.0400" Y="0.1600" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="txt_redshoes" ActionTag="1907287392" CallBackType="Touch" UserData="txt_redshoes" Tag="74" IconVisible="False" LeftMargin="154.0023" RightMargin="37.9977" TopMargin="110.7090" BottomMargin="57.2910" FontSize="14" LabelText="0&#xA;" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="8.0000" Y="32.0000" />
                        <AnchorPoint ScaleX="0.4116" ScaleY="0.2751" />
                        <Position X="157.2951" Y="66.0942" />
                        <Scale ScaleX="2.1154" ScaleY="0.3079" />
                        <CColor A="255" R="0" G="0" B="0" />
                        <PrePosition X="0.7865" Y="0.3305" />
                        <PreSize X="0.0400" Y="0.1600" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="txt_coins" ActionTag="1255587959" CallBackType="Touch" UserData="txt_coinsEvent" Tag="111" IconVisible="False" LeftMargin="101.0261" RightMargin="43.9739" TopMargin="13.1378" BottomMargin="134.8622" FontSize="10" LabelText="Hello World&#xA;&#xA;&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="55.0000" Y="52.0000" />
                        <AnchorPoint ScaleX="0.5064" ScaleY="0.6139" />
                        <Position X="128.8781" Y="166.7850" />
                        <Scale ScaleX="0.6792" ScaleY="0.2193" />
                        <CColor A="255" R="0" G="0" B="0" />
                        <PrePosition X="0.6444" Y="0.8339" />
                        <PreSize X="0.2750" Y="0.2600" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position X="2.6653" Y="67.2334" />
                    <Scale ScaleX="1.0000" ScaleY="1.1626" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0133" Y="0.2241" />
                    <PreSize X="1.0000" Y="0.6667" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="37.4246" Y="-283.1583" />
                <Scale ScaleX="0.6886" ScaleY="2.3983" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1871" Y="-1.4158" />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="255" G="150" B="100" />
                <FirstColor A="255" R="255" G="150" B="100" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
                <InnerNodeSize Width="200" Height="300" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="9.6338" ScaleY="5.4353" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.1042" Y="0.1852" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_back" ActionTag="2138858367" UserData="btn_back" Tag="50" IconVisible="False" LeftMargin="65.4976" RightMargin="1731.5024" TopMargin="947.8807" BottomMargin="9.1193" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="93" Scale9Height="101" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="123.0000" Y="123.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="126.9976" Y="70.6193" />
            <Scale ScaleX="1.1887" ScaleY="0.9277" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0661" Y="0.0654" />
            <PreSize X="0.0641" Y="0.1139" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="btn/button_artbord-03.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>