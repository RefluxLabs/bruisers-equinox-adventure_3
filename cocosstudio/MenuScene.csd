<GameFile>
  <PropertyGroup Name="MenuScene" Type="Scene" ID="2683151f-d310-4c83-9041-c689c4fbf1d6" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="600" Speed="1.0000">
        <Timeline ActionTag="892781505" Property="Position">
          <PointFrame FrameIndex="0" X="1959.5939" Y="537.4327">
            <EasingData Type="10" />
          </PointFrame>
          <PointFrame FrameIndex="600" X="-35.0168" Y="534.2815">
            <EasingData Type="6" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-1934218977" Property="Position">
          <PointFrame FrameIndex="0" X="-50.0000" Y="85.0348">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="600" X="2000.0000" Y="85.0348">
            <EasingData Type="1" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-1934218977" Property="FileData">
          <TextureFrame FrameIndex="0" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="5" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="10" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="15" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="20" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="25" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="30" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="35" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="40" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="45" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="50" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="55" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="60" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="65" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="70" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="75" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="80" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="85" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="90" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="95" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="100" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="105" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="110" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="115" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="120" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="125" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="130" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="135" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="140" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="145" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="150" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="155" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="160" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="165" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="170" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="175" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="180" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="185" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="190" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="195" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="200" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="205" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="210" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="215" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="220" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="225" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="230" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="235" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="240" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="245" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="250" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="255" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="260" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="265" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="270" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="275" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="280" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="285" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="290" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="295" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="300" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="305" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="310" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="315" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="320" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="325" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="330" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="335" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="340" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="345" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="350" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="355" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="360" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="365" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="370" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="375" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="380" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="385" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="390" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="395" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="400" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="405" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="410" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="415" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="420" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="425" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="430" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="435" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="440" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="445" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="450" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="455" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="460" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="465" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="470" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="475" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="480" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="485" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="490" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="495" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="500" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="505" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="510" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="515" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="520" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="525" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="530" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="535" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="540" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="545" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="550" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="555" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="560" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="565" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="570" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="575" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_001.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="580" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_002.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="585" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_003.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="590" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_004.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="595" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_005.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="600" Tween="False">
            <TextureFile Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
          </TextureFrame>
        </Timeline>
      </Animation>
      <AnimationList>
        <AnimationInfo Name="anim_bgScroll" StartIndex="0" EndIndex="300">
          <RenderColor A="255" R="255" G="192" B="203" />
        </AnimationInfo>
        <AnimationInfo Name="animation0" StartIndex="300" EndIndex="600">
          <RenderColor A="255" R="138" G="43" B="226" />
        </AnimationInfo>
        <AnimationInfo Name="animation_run" StartIndex="0" EndIndex="30">
          <RenderColor A="150" R="0" G="0" B="255" />
        </AnimationInfo>
      </AnimationList>
      <ObjectData Name="MenuScene" CustomClassName="MenuScene" UserData="MenuScene enabled" Tag="4" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="892781505" Alpha="204" Tag="8" IconVisible="False" LeftMargin="-1969.0168" RightMargin="21.0168" TopMargin="5.7185" BottomMargin="-5.7185" Scale9Enable="True" LeftEage="1276" RightEage="1276" TopEage="356" BottomEage="356" Scale9OriginX="1276" Scale9OriginY="356" Scale9Width="1316" Scale9Height="368" ctype="ImageViewObjectData">
            <Size X="3868.0000" Y="1080.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-35.0168" Y="534.2815" />
            <Scale ScaleX="1.0155" ScaleY="1.0155" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.0182" Y="0.4947" />
            <PreSize X="2.0146" Y="1.0000" />
            <FileData Type="Normal" Path="environment1/BG_1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_play" ActionTag="1220279320" CallBackType="Touch" CallBackName="btn_play" UserData="play button touched" Tag="1" IconVisible="False" LeftMargin="883.7262" RightMargin="913.2738" TopMargin="470.8811" BottomMargin="486.1189" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="93" Scale9Height="101" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="123.0000" Y="123.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="945.2262" Y="547.6189" />
            <Scale ScaleX="2.3819" ScaleY="2.3819" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4923" Y="0.5071" />
            <PreSize X="0.0641" Y="0.1139" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="btn/button_artbord-01.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_highscore" ActionTag="-1699095961" CallBackType="Touch" CallBackName="btn_highscore" UserData="highscore_touched" Tag="2" IconVisible="False" LeftMargin="21.5504" RightMargin="1775.4496" TopMargin="903.5612" BottomMargin="53.4388" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="93" Scale9Height="101" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="123.0000" Y="123.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="83.0504" Y="114.9388" />
            <Scale ScaleX="1.1385" ScaleY="1.1385" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0433" Y="0.1064" />
            <PreSize X="0.0641" Y="0.1139" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="btn/button_artbord-02.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_1" ActionTag="-1934218977" Tag="114" IconVisible="False" LeftMargin="1877.7894" RightMargin="-105.7894" TopMargin="808.1648" BottomMargin="62.8352" ctype="SpriteObjectData">
            <Size X="148.0000" Y="209.0000" />
            <AnchorPoint ScaleX="0.8257" ScaleY="0.1062" />
            <Position X="2000.0000" Y="85.0348" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.0417" Y="0.0787" />
            <PreSize X="0.0771" Y="0.1935" />
            <FileData Type="PlistSubImage" Path="Run/Run_000.png" Plist="Hero.Bruiser/0.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_store" ActionTag="-1190404352" CallBackType="Touch" UserData="btn_store touched!" Tag="3" IconVisible="False" LeftMargin="1746.6069" RightMargin="50.3931" TopMargin="904.5594" BottomMargin="52.4406" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="93" Scale9Height="101" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="123.0000" Y="123.0000" />
            <AnchorPoint ScaleX="0.4591" ScaleY="0.3217" />
            <Position X="1803.0763" Y="92.0097" />
            <Scale ScaleX="1.1242" ScaleY="1.1242" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9391" Y="0.0852" />
            <PreSize X="0.0641" Y="0.1139" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="btn/button_artbord-04.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>