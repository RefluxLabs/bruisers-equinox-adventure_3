/**
 * Play scene.
 * <p>
 * The scene manages the game objects and the play.
 * </p>
 *
 * @class
 * @extends cc.Scene
 *
 * @property {Object} space the space object.
 */
var PlayScene = cc.Scene.extend(/** @lends PlayScene# */{
    /**
     * Constructor of cc.Scene
     */
    _className: "PlayScene",

    /**
     * The physic space object.
     */
    space: null,

    /**
     * The camera of the scene.
     */
    camera: null,

    /**
     * The statistics.
     */
    statistics: null,

    /**
     * The settings.
     */
    settings: null,

    /**
     * The game state.
     */
    state: null,
    /**
     * The game state.
     */

    goal:null,
    /**
     *judge beAte
     */
    beAte: false,

    _initState: function () {
        var gameover = false;
        var self = this;
        return {
            get gameover() {
                return gameover;
            },
            set gameover(g) {
                gameover = g;
            }
        }
    },

    _initSpace: function () {
        var space = new cp.Space();
        space.gravity = cp.v(0, -1500);

        // the ground.
        var wallBottom = new cp.SegmentShape(
            space.staticBody,
            // Start point
            cp.v(0, res.physics.groundHeight),
            // MAX INT:4294967295
            cp.v(4294967295, res.physics.groundHeight),
            // thickness of wall
            0);
        wallBottom.setCollisionType(SpriteTag.ground);
        space.addStaticShape(wallBottom);

        return space;
    },

    _initCamera: function () {
        var winSize = cc.director.getWinSize();
        var centerPos = cc.p(-winSize.width / 2, 0);
        var destPos = null;
        var listeners = [];
        var dirty = false;
        return {
            addListener: function (listener) {
                listeners.push(listener);
            },
            set x(x) {
                centerPos.x = x;
                dirty = true;
            },
            get x() {
                return centerPos.x;
            },
            set y(y) {
                centerPos.y = y;
                dirty = true;
            },
            get y() {
                return centerPos.y;
            },
            set pos(pos) {
                centerPos.x = pos.x;
                centerPos.y = pos.y;
                dirty = true;
            },
            update: function () {
                if (dirty) {
                    listeners.forEach(function (listener) {
                        listener(centerPos);
                    });
                    dirty = false;
                }
            }
        };
    },

    _initStatistics: function () {
        var self = this;
        var debugGOS = false;
        var coins = 0;
        var kill = 0;
        var bird = 0;
        var BestDistance = sys.localStorage.getItem("BestDistance");
        var death = 0;//@lends##play-scene
        var d = 20;
        var L = 10;
        var lives = 5;
        var sl = 10;
        var level = 0;//model: Shared variable!
        var clock = 0;
        var stars = 0;

        var status = {
            enabled: true,
            stars: 3,
            level: 1,
        };

        //todo set up goal objects <- system.localS..
        var goalMeter10 = sys.localStorage.getItem("goalMeter10");
        //var goalMeter10E;

        var goalMeter20 = sys.localStorage.getItem("goalMeter20");
        //var goalMeter20E;

        var goalMeter30 = sys.localStorage.getItem("goalMeter30");
        //var goalMeter30E;

        return {
            set stars(s) {

                stars = s;
            },
            get stars() {
                return stars;
            },
            set statusStars(s) {
                status.stars = s;
            },
            get statusStars() {
                return status.stars;
            },
            set statusLevel(sl) {
                if(this.stars > 1){
                    sl = 10;
                }else{
                    sl = 0;
                }
                this.level = sl;
            },
            get statusLevel() {
                return status.level;
            },
            set statusEnabled(t) {
                if (t) {
                    status.enabled = true;
                } else {
                    status.enabled = false;
                }
            },
            get statusEnabled() {
                return status.enabled;
            },
            set coins(c) {
                coins = c;
            },
            get coins() {
                return coins;
            },
            set kill(k) {
                kill = k;
            },
            get kill() {
                return kill;
            },
            set bird(b) {
                bird = b;
            },
            get bird() {
                return bird;
            },
            get score() {
                return this.coins + this.meter;
            },
            get meter() {
                return parseInt(self.camera.x / 100);
            },
            set BestDistance(num) {
                this.num = num;
                sys.localStorage.setItem("BestDistance",this.num);
            },
            get BestDistance() {
               return parseInt(sys.localStorage.getItem("BestDistance"));
            },
            set death(d) {
                death = d;
            },
            get death() {
               return death;
            },
            set lives(L) {
                lives = L;
            },
            get lives() {
                return lives;
            },
                    /**Todo
                      *set level(ls) works 8/6,
                      * task: instead od comparing variables -- add level States
                      * which would be declared within resource.js
                      */
            set level(ls) {
                if(this.stars >= 0){
                ls = 10;
                }else {
                ls = 111;
                }
                this.level = ls;
            },
//            |||||||||||||||||||||||||||||||||||
//          set cookies(value) {
//        if (value >= 0 && value <= 10) {
//        this.numberOfCookies = value;
//        } else {
//        throw new Error("Please use a number between 0 and 10");}
//    }
//            |||||||||||||||||||||||||||||||||||
            get level() {
                return level;
            },
            set clock(x) {
                    clock = x;
            },
            get clock() {
                return clock;
            },
            transverse: function(){
            //skip inheritance
              for(var key in status) {
                    if (status.hasOwnProperty(key)) {
                        console.log(key);
                    }
                }

            //// Visit non-inherited enumerable keys
            //    Object.keys(obj).forEach(function(key) {
            //        console.log(key);
            //    });
            },
            Callback_fn: function () {
                cc.log("callback_fn met >> play-scene.js");
            },
            update: function(){

/*todo Task #1 goals
 *  Logic for bruiser's goals with the dependency of distance
 *  Three functions goalMeter10, goalMeter20, goalMeter30
 *  Will require a special variable ending in 'E' to establish a single loop!
 * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
 *but perhaps the creation of the new model will
 * be not some crack head multi-variable if then statements but
 * a well design abstration concept of function generators
**/
            goalMeter10 = sys.localStorage.getItem("goalMeter10");
            //sys.localStorage.setItem("goalMeter10","undefined");
            cc.log(goalMeter10);
              if((typeof _goalMeter10E == "undefined") && (goalMeter10 == 0))                    {//local condition
                    if(this.meter >= 5){
                        this.stars++;//stars go from # 1->2
                        _goalMeter10E = true;//required so that this loops only Once!
                        sys.localStorage.setItem("goalMeter10", 1);//set item to true/
                    }
            }
            try{
            goalMeter20 = sys.localStorage.getItem("goalMeter20");
            //sys.localStorage.setItem("goalMeter10","undefined");
            cc.log(goalMeter20);
              if((typeof _goalMeter20E == "undefined") && (goalMeter20 == 0))
                    {//local condition
                    if(this.meter >= 30){
                    this.stars++;//stars go from # 1->2
                    _goalMeter20E = true;//required so that this loops only Once!
                    sys.localStorage.setItem("goalMeter20", 1);//set item to true/
                    }
            }
            }catch (error){cc.log("goalMeter20:" + error);}
        //|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\
        // Todo Test this module set 3 below! will be the third goal!
            goalMeter30 = sys.localStorage.getItem("goalMeter30");
            cc.log(goalMeter30);
          if((typeof _goalMeter30E == "undefined") && (goalMeter30 == 0))
          {
                if(this.meter >= 90){
                    this.stars++;//stars go from # 1->2
                    _goalMeter30E = true;//required so that this loops only Once!
                    sys.localStorage.setItem("goalMeter30", 1);//set item to true/
                }
          }
        //cc.log(Object.keys(statistics));
/////////////////////////////////////////////////////////////////////////////
            }//EOF-> Update
        };//EOF return

    },//EOF Function

    _initSettings: function () {
        var audio = {
            enabled: false,
            volume: 1
        };
        var video = {
            quality: 0
        }
        return {
            set audioVolume(v) {
                audio.volume = v;
            },
            get audioVolume() {
                return audio.volume;
            },
            set audioEnabled(t) {
                if (t) {
                    // add background music
                  //  cc.audioEngine.playMusic(res.sound.bg_mp3, true);
                    audio.enabled = true;
                } else {
                    cc.audioEngine.stopMusic();
                    audio.enabled = false;
                }
            },
            get audioEnabled() {
                return audio.enabled;
            },
            set videoQuality(quality) {
                if (quality) {
                    var particle = new cc.ParticleSystem(res.particle.circle);
                    particle.setPosition(800, 100);
                    this.addChild(particle, 100);
                }
                video.quality = quality;
            },
            get videoQuality() {
                return video.quality;
            }
        }
    },

    /**
     * Invoke on scheduled update.
     */
    update: function (dt) {
        if (!this.state.gameover) {
            this.space.step(dt);
            // Update the layers.
            // Don't schedule update in layers itself in case of delay of the physics.
            this.playLayer.update();
            this.camera.update();
            this.statistics.update();

        }


    },

    onExit: function () {
        this.unscheduleUpdate();
        this._super();
    },

    onEnter: function () {
        this._super();
        // the initial variables.
        var space = this.space = this._initSpace();
        var camera = this.camera = this._initCamera();
        var statistics = this.statistics = this._initStatistics();
        var settings = this.settings = this._initSettings();
        var state = this.state = this._initState();
        var self = this;

        ////////////////////////////////////////////
        //todo: Debug GameOverlayer!!
        var debugGOS = this.debugGOS = false;
        ////////////////////////////////////////////
        // default settings
        settings.videoQuality = 0;
        settings.audioEnabled = false;

//////////////////////////////////////////////////////////////////////
//TODO: Parallax Node Algorthim!!!
/**
 *Task #5 issue: fix so that when brusier jumps the a. camera follows him, b. background descales
 * according to bruiser!
 ** solution actually doesn't lie here! it lies within the play layer
 * instance of the bg nodes->parallax nodes
 */
//var Parallax();
////////////////////////////////////////////////////////////////////////

        this.addChild(new RepeatBackgroundLayer(camera, res.background[2], {
            scaleX: 2,//2
            scaleY: 10//10
        }));
        this.addChild(new RepeatBackgroundLayer(camera, res.background[3], {
            scaleX: 3,//3
            scaleY: 10//10
        }));

        var playLayer = this.playLayer = new PlayLayer(camera, space, statistics, settings);
        this.addChild(playLayer);

        var layer = this.hudLayer = new HudLayer(playLayer.player, statistics, camera, settings);
        this.addChild(layer);

        if(debugGOS){
            state.gameover = true;
            self.addChild(new StatisticsLayer(statistics));
        }

        // Setup collision Handler
        space.addCollisionHandler(
            SpriteTag.player,
            SpriteTag.gold,
            function (arbiter, space) {
                var shapes = arbiter.getShapes();
                var coin = shapes[1];
                statistics.coins += 1;
                coin.body.spriteObj.removeFromLayer();
                coin.body.spriteObj.available = false;
            }, null, null, null);

            //////////////////////////////////////////////
            //todo add collsion handler for new generator
            //bird collision handler
            space.addCollisionHandler(
            SpriteTag.player,
            SpriteTag.bird,
            function (arbiter, space){
                var shapes = arbiter.getShapes();
                var bird = shapes[1];
                statistics.kill += 1;

                //judge eat or die
                if(playLayer.player.status == 'jump') {
                    //old mechanism
                    //this.shapesToRemove.push(shapes[1]);
                    //this.playLayer.birdGenerator.bird.removeFromLayer();

                //TODO  #new singleton mechanism
                 bird.body.spriteObj.removeFromLayer();
                //bird.body.spriteObj.available = false;

                //play frog music
                cc.audioEngine.playEffect(res.sound.enemyDied);
                statistics.kill += 1;
                }else{
                    this.beAte = true;//todo: create meaning for this
                    bird.body.spriteObj.removeFromLayer();
                    //statistics.death += 1;
                    //TODO: initialize state of game-over-layer.js/////
                    //state.gameover = true;
                    //self.addChild(new StatisticsLayer(statistics));
                }
            }.bind(this), null, null, null);
////////////////////////////////////////////////////////////////////////////
//Todo *** spring collision handler
		space.addCollisionHandler(
				SpriteTag.player,
				SpriteTag.spring,
				function(arbiter, space) {
                    var shapes = arbiter.getShapes();
                    var spring = shapes[1];
                    //statistics.spring +=1;
                    spring.body.spriteObj.removeFromLayer();
                    playLayer.player.getSpring();
                        this.scheduleOnce(function(){
                            playLayer.player.loseSpring();
                        }.bind(this),1300);
                cc.audioEngine.playEffect(res.sound.spring);
				}.bind(this), null, null, null);
/////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////
//todo *** frog collision handler
//		space.addCollisionHandler(
//				SpriteTag.player,
//				SpriteTag.frog,
//				function(arbiter, space) {
//				var shapes = arbiter.getShapes();
//				var frog = shapes[1];
//				//statistics.frog +=1;
//
//                //judge eat or die
//                if(playLayer.player.status == 'jump') {
//                    //this.gameLayer.frogGenerator.frog.removeFromLayer();
//                     frog.body.spriteObj.removeFromLayer();
//
//                    //play frog music
//                    cc.audioEngine.playEffect(res.sound.enemyDied);
//                    statistics.kill++;
//                }else{
//                    this.beAte = true;
//                    //this.shapesToRemove.push(shapes[1]);
//                     frog.body.spriteObj.removeFromLayer();
//                  }
//
//				}.bind(this), null, null, null);
/////////////////////////////////////////////////////////////////////

        // Setup game over detection
        space.addCollisionHandler(
            SpriteTag.player,
            SpriteTag.ground,
            function (arbiter, space) {
                statistics.lives--;
                statistics.death += 1;
                state.gameover = true;
                self.addChild(new StatisticsLayer(statistics));
            }, null, null, null);

        space.addCollisionHandler(
            SpriteTag.player,
            SpriteTag.inventory,
            function (arbiter, space) {
                var shapes = arbiter.getShapes();
                var inventory = shapes[1].body.spriteObj;
                var hero = shapes[0].body.spriteObj;
                inventory.equip(hero, this);
            }.bind(this), null, null, null);

        // schedule the updates.//////////////////////
        this.scheduleUpdate();

    },
        //todo: model: collisionHandler
        // this function is not in working order!
    	collisionBird: function(arbiter, space) {
    		var shapes = arbiter.getShapes();
    		var bird = shapes[1];
            //statistics.coins += 1;
            bird.body.spriteObj.removeFromLayer();
            //bird.body.spriteObj.available = false;
        		//judge eat or die
                //if(this.playLayer.player.status == 'down') {
                //    this.shapesToRemove.push(shapes[1]);
                //    this.playLayer.birdGenerator.bird.removeFromLayer();
                //    //play frog music
                //    cc.audioEngine.playEffect(res.sound.enemyDied);
                //    statistics.kill++;
                //}else{
                //    this.beAte = true;
                //    this.shapesToRemove.push(shapes[1]);
                //}

           state.gameover = true;
           self.addChild(new StatisticsLayer(statistics));
    	},

    addInventoryIndicator: function (ind) {
        this.hudLayer.addIndicator(ind);
    },

    removeInventoryIndicator: function (ind) {
        this.hudLayer.removeIndicator(ind);
    }

});