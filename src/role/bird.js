var Bird = cc.Class.extend({

    // renderer related.
    spriteSheet: null,
    sprite: null,

    // physics related.
    space: null,
    body: null,
    shape: null,
    runningSpeed: -10,
    x: 0,
    y: 0,
    /**
      * Construct a new bird.
      */
    ctor: function (posX, posY, statistics, runningSpeed) {
        this.x = posX;
        this.y = posY;
        this.statistics = statistics;
        if(runningSpeed)    {
            this.runningSpeed = runningSpeed;
        }

        this.spriteSheet = new cc.SpriteBatchNode(res.bird.png);
        this.birdAction = new cc.RepeatForever(new cc.Animate(
        new cc.Animation([1, 2, 3, 4, 5].map(function (i) {
        return cc.spriteFrameCache.getSpriteFrame("bird_0" + i + ".png");
            }), 0.15)
        ));
        this.birdAction.retain();
        this.sprite = new cc.PhysicsSprite("#bird_01.png");
        //this.sprite.setScale(0.8);
        this.spriteSheet.addChild(this.sprite);
        this.sprite.runAction(this.birdAction);
        this.sprite.retain();
        this.spriteSheet.retain();

        //get random speed
        this.runningSpeed = -parseInt(Math.random() * 30 + 10);

        //physics
        var contentSize = this.sprite.getContentSize();
        var body = new cp.Body(0.1, cp.momentForBox(Number.POSITIVE_INFINITY, contentSize.width, contentSize.height));
        body.applyImpulse(cp.v(this.runningSpeed, 0), cp.v(0, 0));
        body.applyForce(cp.v(0, 150), cp.v(0, 0));
        body.setPos(cc.p(posX, posY));
        this.body = body;
        this.sprite.setBody(body);
        //todo: declare .spriteObj
        body.spriteObj = this;

        var shape = new cp.BoxShape(body, contentSize.width - 14, contentSize.height - 18);
        this.shape = shape;
        this.shape.setElasticity(0);
        this.shape.setCollisionType(SpriteTag.bird);
        //Sensors only call collision callbacks, and never generate real collisions
        this.shape.setSensor(true);
    },

    addToLayer: function (space, layer) {
        this.layer = layer;
        layer.addChild(this.spriteSheet,7);//may need local z order here!
        //todo:layer.addChild(this.spriteSheet, 7);
        this.space = space;
        space.addBody(this.body);
        space.addShape(this.shape);
    },
////////////////////////////////////////////////////////////
// todo removefromLayer lends self->collisionHandler!
//////////////////////////////////////////////////////////
    removeFromLayer: function () {
            var px = this.sprite.getPositionX();
            var py = this.sprite.getPositionY();
            var action = (new cc.MoveTo(0.5, cc.p(px - 100, py -                                              500))).easing(cc.easeBackIn());
            this.sprite.runAction(action);

    },

    update: function (dt) {
        var statistics = this.statistics;
        //todo: ___ forEach bird that renders!!
        //        forEach(function (b) {
        //            //var bpos = b.sprite.getPosition();
        //            statistics.bird++;
        //        });
        //||||||||||||||||||||||||||||||||||||||||||||||||
        //todo: ___ forEach bird that dies or gets eaten!!
        //        forEach(function (b) {
        //            //var bpos = b.sprite.getPosition();
        //            statistics.Kill++;
        //        });
        //||||||||||||||||||||||||||||||||||||||||||||||||||
        //todo: ___ forEach bird that wins!!
        //        forEach(function (b) {
        //            //var bpos = b.sprite.getPosition();
        //            statistics.birdwins++;
        //        });
        ///////////////////////////////////////////////////////
    },

    initPos: function (pos) {
            this.sprite.stopAllActions();
          //  this.sprite.runAction(this.rotatingAction);
            this.sprite.setPosition(pos);
            this.available = true;
    },


    getX: function () {
        return this.sprite.getPositionX();
    },

    getName: function () {
        return "bird";
    },

    getShape: function () {
        return this.shape;
    }
});