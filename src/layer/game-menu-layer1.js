var GameMenuLayer1 = cc.Layer.extend(/**@lends GameMenuLayer# */{
    /**
     * Initialize the application menu layer Cocos Studio
     */
    openStore: false,
    ctor: function (pageviewSceneClass) {
        this._super();
        var winsize = cc.director.getWinSize();
        var size = cc.winSize;
        ///////////////////////////////////////////
        if (!pageviewSceneClass){
        pageviewSceneClass = PageViewScene;
        }
        this.pageviewSceneClass = PageViewScene;
        ////////////////////////////////////////////////////////////
        var menuscene = ccs.load(res.IAP.MenuScene_json);
             var action = menuscene.action;
                    if(action){

                        menuscene.node.runAction(action);

                        action.gotoFrameAndPlay(0, true);
                    }
            //todo work on parse theory!
            //              var ui = menuscene.ui;
            //                if (ui){
            //                    menuscene.node
            //                }
            //             var Class = menuscene.Class;
            //                if(Class){
            //                    ccs.objectFactory.createObject("GameOverClass");
            //                }
        this.addChild(menuscene.node);
        cc.log("menu scene loaded!");
        //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        var btn_play = menuscene.node.getChildByTag(1);
        var userdata = btn_play.getUserData();
        cc.log(userdata);
        var btn_highscore = menuscene.node.getChildByTag(2);
        cc.log(btn_highscore);
        var btn_store = menuscene.node.getChildByTag(3);
        cc.log(btn_store);
        btn_play.addTouchEventListener( this.touchEvent, this );
        btn_highscore.addTouchEventListener( this.btn_highscoreEvent, this );
        btn_store.addTouchEventListener(this.btn_storeEvent,this);
    },
    touchEvent: function(sender, type)
    {
        switch (type)
        {
        case ccui.Widget.TOUCH_BEGAN:
            break;
        case ccui.Widget.TOUCH_MOVED:
            break;
        case ccui.Widget.TOUCH_ENDED:
            cc.log("btn_play touch ended!")
            var scene = new PlayScene();
            cc.director.pushScene(scene);
            break;
        case ccui.Widget.TOUCH_CANCELLED:
            break;
        }
    },
        btn_highscoreEvent: function(sender, type)
        {
            switch (type)
            {
            case ccui.Widget.TOUCH_BEGAN:
                break;
            case ccui.Widget.TOUCH_MOVED:
                break;
            case ccui.Widget.TOUCH_ENDED:
            //////////////////////////////////////////
            //**If debugging look at context.js **//
            //also look at game over layer.js playSceneClass
//
                cc.log("PageView --- touch ended!");
                cc.log("below I call pageviewScene() from a Global point!")
            cc.director.runScene(new this.pageviewSceneClass());
 //       cc.director.runScene(new StatisticsLayer(statistics));
        //this.addChild(new StatisticsLayer(statisitcs));
                break;

            case ccui.Widget.TOUCH_CANCELLED:
                break;
            }
        },
                btn_storeEvent: function(sender, type)
                {
                    switch (type)
                    {
                    case ccui.Widget.TOUCH_BEGAN:
                        break;
                    case ccui.Widget.TOUCH_MOVED:
                        break;
                    case ccui.Widget.TOUCH_ENDED:
                               cc.log("btn_open store touch ended!")
                        cc.audioEngine.stopMusic();
//                        var scene = new StoreScene();
                        var scene = new SdkboxPlayScene();
                        cc.director.pushScene(scene);
                        break;
                    case ccui.Widget.TOUCH_CANCELLED:
                        break;
                    }
                }

});