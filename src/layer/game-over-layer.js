///////////////////////////////////////////////
//Game Over Layer = Statistics Layer in
//statistics-layer.js and alternative would be to go to
//bitbucket before gameoverlayer commit and look at the file
//game-over-layer.js <- this file holds data without the upload functionality
///////////////////////////////////////////////

var StatisticsLayer = cc.LayerColor.extend(/**@lends StatisticsLayer# */{
	_className:"GameOverClass",
	newClock : 0,
	time : 10,
	statistics: null,
	GameOverStats: null,
	_print: 0,
	print:0,
	cval : 0, //todo ** play-scene() collision handler writes to statistics.coins!
	Bcval : 0,//comparision in update() for cval
	sval : 0,
	kval : 0,
	mval : 0,
	Bsval : 0,
	sval : 0,
	kval : 0,
	mval : 0,
	bsVal : 0,//"BestScore"
	leVal: 0,//Levels
	liVal: 4,//Lives
	dval : 0,//"TotalDeath"
	jval : 0,//"TotalJump"
    beVal : 0,//"TotalBirdsEn"
    bkVal : 0,//"TotalBirdsKilled"
    bdVal : 0,//"BestDistance"
    highScoreVal : 49,//"HighScore"
    deathNum : 10,
    hsVal : 0,
    previousScoreVal : 0,
    currentScoreVal : 0,
	room: null,
	openStats: true,
	openStore: false,
	openData: true,
	onData: null,
	labelScore1: null,
	labelHighScore: null,

	labelCoin1: null,
	labelBestCoin: null,
	labelTotalCoin: null,

	LabelDistance1: null,
	labelBestDistance: null,
	labelTotalDistance: null,

	labelLevel: null,

    countMeter: 0,
    loadingBar: null,

    goalMeter10: null,
	statisticsArr: [],
	dap: [],
	// constructor
	ctor:function (statistics, playSceneClass) {
		this._super();
		this.init(cc.color(0, 0, 0, 80));
		if (! playSceneClass) {
			playSceneClass = PlayScene;
		}
		////////////////////////////////////////////////////////////////////////
		//todo-> Object.keys
		// third best way to solve the problem
		// Put the enumerable properties and methods of the object in an array.
        var arr = Object.keys(statistics);
        cc.log("ARR:" + arr);
		var dap = new Array();
		var sources = arr;
		var ext  = cc.extend(dap,sources);
		//cc.extend(dap,sources);
		//cc.log("dap:" + dap);
		cc.log("ext");
		cc.log(ext);
		var cheese = cc.isArray(ext);
		var cheese2 = cc.isArray(dap);
		cc.log("cheese", cheese);
		cc.log("cheese2", cheese2);
		cc.log(dap);
		cc.log("+++==dap[2]...is below");
		cc.log(dap[2]);//>> 'statuslevel'

//you should first filter pushing the filters objects & values to new arr
//then next step is to iterate through new arr testing items 'coins & meter'
// if they passed a certain range function returning true or false and also incrementing +1 //star
//
//-----------------
//
//now you can count this.stars and declare what the current level is based on the number of
//stored stars!
//////////////////////////////////////////////////////
//todo //forEach.obj[key]
//	1. check if key is '' certain key
// if dap[4].some(this arg, RangeCallback)
//	 1.2 call range callback function returning boolean
//	 1.2 //call function to push or update 'this.stars'
function isLessThan20(element,index,array) {
	return element < 3;
}
this.info = [1,2,3,4].some(isLessThan20);//true
//this.inf = dap.some(isLessThan20);//false bc 'dap' should be [#]
cc.log("dap.some(isLessThan20)....below")
cc.log(this.info);

/////////////////////////////////////////////////////////
//Mozilla Example  some function
//function isBiggerThan10(element, index, array) {
//  return element > 10;
//}
//[2, 5, 8, 1, 4].some(isBiggerThan10);  // false
//[12, 5, 8, 1, 4].some(isBiggerThan10); // true
//////////////////////////////////////////////////////////////////////
//todo cc.extend
// third best way to solve the problem
//cc.extend copies sources data into new Array() of 'dap'
//you may now use array functions to manipulate the logic
//according to R and D you could of used array functions with the original object as well !!!
        //var obj = { first: "John", last: "Doe" };
            // Visit non-inherited enumerable keys
//Todo:    Object.keys(statistics).forEach(function(key) {
            Object.keys(statistics).forEach(function(key) {
            var value = statistics[key];
            //_newArr.push(value)//second array to hold stored values of the keys
//            cc.log("-----key-------");
//            cc.log(key);
//            cc.log("----value-----");
//            cc.log(value);
              cc.log(key, "->", value);

            });
//            Object.keys(obj).forEach(function (key) {
//              var value = obj[key];
//               // do something with key or value
//            });
/////////////////////////////////////////////////////
////////////////////////////////////////////////////
//var countries = {
//    China: 1371980000,
//    India: 1276860000,
//    'United States': 321786000,
//    Indonesia: 255461700,
//    Brazil: 204873000,
//    Pakistan: 190860000
//};
/////////////////////////////////////////////////////
// ** Range Function **
//if (x >= 0.001 && x <= 0.009) {
//  // something
//}
//You could write yourself a "between()" function:
//
//function between(x, min, max) {
//  return x >= min && x <= max;
//}
//// ...
//if (between(x, 0.001, 0.009)) {
//  // something
//}

function between(x,min, max) {
return x >= min && x <= max;
}
////////////////////////////////////////////////
//TODO ** organize this shit!!!
// Test function.chrome
//////////////////////////////////////////////
//method #1 global
//1.declare callback_fn
//2.conditional if statement
//2.1 usage of .some( w/ callback_fn)
function isReallyBig(element , min, max) {
	var value = statistics['meter'];
	var min = 0;
	var max = 40;
    return (value >= min && value <= max);
}
// Using the countries array the following condition should return true
if (Object.keys(statistics).some(isReallyBig)) {
    cc.log('Object statistics contains numbers in between 0 - 7');
	statistics.stars += 200;
}
////////////////////////////////////////////////////
   //#1 here only difference is that our callback_fn is more specific calling element 'meter'
//todo func declaration
function mt(element) {
		/*
		* if statistics.meter is greater than the best distance by +30 gains
		* compute >> func is validated to true!
		* thus now your 'if condition' should return results of #1 'star' to this.stars
		**/

		if(statistics['meter'] > (this.bestDistanceNum + 5)){ // stat.meter needs to be greater than best distance users ran + gains of 30//
		//return statistics['meter']
		return true;

		}else return false;
//		return statistics['meter'] > 30;
		// should limit this to be validated only on best run number allowing for no repeats
		//where is my logic for best meter?
}
///////////////////////////////////////////////////////////////
//function validateTextNumericInRange(textInputId, min, max) {
//    var textInput = document.getElementById(textInputId);
//    var value = parseInt(textInput.value, 10);//would not need to do bc element is already written with a value
//    return (!isNaN(value) && value >= min && value <= max);
//}
////////////////////////////////////////////////////////////
function validateNumericInRange(element, min, max){
	var value = statistics[element];
    return (!isNaN(value) && value >= min && value <= max);
}
var valid = validateNumericInRange('score',0,45);
cc.log(valid);
cc.log("score || valid fn above ..");
///////////////////////////////////////////////////////////////
//# 2 conditional if statement >> out a star if true!
if(Object.keys(statistics).some(mt)) {
	statistics.stars++;
	cc.log("statistics.meter at pos meter, is valid for > +30: stars++");
}

////////////////////////////////////////////////////
//todo *** Loop through ArrayObject with condition
///////////////////////////////////////////////////
//var arrayObjects = [{"building":"A", "status":"good"},{"building":"B","status":"horrible"}];
//
//for (var i=0; i< arrayObjects.length; i++) {
//  console.log(arrayObjects[i]);
//
//  for(key in arrayObjects[i]) {
//
//      if (key == "status" && arrayObjects[i][key] == "good") {
//
//          console.log(key + "->" + arrayObjects[i][key]);
//      }else{
//          console.log("nothing found");
//      }
//   }
//}
////////////////////////////////////////////////////////////////////////
//** Only works in ES5////
///////////////////////////////////////////////////////
//		Object.keys(source).forEach(function (prop) {
//            target[prop] = source[prop];
//        });
//////////////////////////////////////////////////////
//var obj = { first: "John", last: "Doe" };
//
//Object.keys(obj).forEach(function(key) {
//    console.log(key, obj[key]);
//});
////////////////////////////////////////////////////////////////////////
//todo .map function
// second best way
        cc.log(".map -> arrmap below-----");
            var arrmap = Object.keys(statistics).map(function (key) {
                                                return statistics[key];
                                                });
            cc.log(arrmap);

//////////////////////////////////////////////////////////////////////////////
////todo using a generator function
//// best way to solve the problem
//function* entries(statistics) {
//   for (let key of Object.keys(statistics)) {
//     yield [key, statistics[key]];
//   }
//}
//
//for (let [key, value] of entries(statistics)) {
//	//todo #1 find [element]
//	//todo #2 check the element to the given condition
//	if(key == 'coins'){
//		//do something
//	}else if(key == 'meter'){
//		//do something else
//	}else{
//	//default case
//	}
//	//return value
//   console.log(key, "->", value);
//}
//////////////////////////////////////////////////////////////////////////////
		//declare this.variable = variable if used as parameter
		this.playSceneClass = playSceneClass;
		this.statistics = statistics;
		//this.labelObject = labelObject;// do not need to declare!!
		//this.createTestMenu();//gameoverstats.json
		firstInit = false;
        ////////////////////////////////////////
		//** Load Game Over Json! ***
		////////////////////////////////////////
        var gameoverlayer = ccs.load(res.IAP.GameOverLayer_json);
        this.addChild(gameoverlayer.node);
		//this.addChild(gameoverlayer.node,localZOrder, Tag);
		cc.log("loading GameOverLayer.json!");
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
			//cc.audioEngine.playMusic(res.sound.shopping, true);
			////////////////////////////////////////////
			//*** Declare FileNode_1 && FileNode_2
			///////////////////////////////////////////
			//@Param:FileNode_1:tag
			//@Param:FileNode_2:301
			//|||||||||||||||||||||||||||||||||||||
			var GameStats = gameoverlayer.node.getChildByTag(301);
//			this.addChild(GameStats, 12);//<< ERR#Child Already Added !not be added again!!!
			cc.log(GameStats);
			//GameStats.setGlobalZOrder();
			cc.log("GameOverView above!" + GameStats);
			//GameStats.setGlobalZOrder();
			var local = GameStats.getLocalZOrder();
			cc.log(local);
			var nlocal = GameStats.setLocalZOrder(34);
			cc.log(nlocal);
			/*
            sets the global z-index (must by a Number)
            nodes with a lower global z-index are rendered first
            */
            //nodeName.setGlobalZOrder( 5 );
            // gets the global z-index value (returns a Number)
            //nodeName.getGlobalZOrder( );

			//7/23/16 below debug works!!!
			//	    	var labelzorder = GameStats.getGlobalZOrder();
			//			cc.log(labelzorder);//0
			//			GameStats.setGlobalZOrder(20);//20
			//
			//			var newZorder = GameStats.getGlobalZOrder();
			//			cc.log(newZorder);

			//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	    	var GameOverView = gameoverlayer.node.getChildByTag(127);
	    	//this.addChild(this.GameOverView,80,127);
			var locc = GameOverView.getGlobalZOrder();
				cc.log("GlobalZOrder for 'locc' variable" + locc );
				//||||||||||||||||||||||||||||||||||||||||||||||
                var PageView = GameOverView.getChildByTag(107);
                cc.log("pageview << function" + PageView);
				//@param: 109
				//@param:117 = btn
				var panelGoal = PageView.getChildByTag(108);
				cc.log(panelGoal);
				/////////////////////////////////////////////////
				//TODO: ** panel - Goals
				var scrol = panelGoal.getChildByTag(218);
				cc.log(scrol);
				var listV = scrol.getChildByTag(220);
				cc.log(listV);
				this.loadingBar = listV.getChildByTag(134);
				cc.log(this.loadingBar);
			    this.countMeter = 25;
			    this.loadingBar.setPercent(this.countMeter);
			////////////////////////////////////////////////////

				///////////////////////////////////////////////////////////////////////////////////////////////////////////////
				//**TODO statistics scene
	                 var panel_2 = PageView.getChildByTag(109);
	                 cc.log("panel_2 << function!");
						 var btn = panel_2.getChildByTag(117);
						 cc.log(btn);
						 //////////////////////////////////////////
						var panel_21 = panel_2.getChildByTag(118);
						cc.log("panel_2.1 = " + panel_21);
						cc.log(panel_21);
						//////////////////////////////////////////
						//** Derive Scroll View_1: 119
						//||||||||||||||||||||||||||||||||||||||||
						var ScrollView_1 = panel_21.getChildByTag(119);
						cc.log(ScrollView_1);
						cc.log("scroll view above!");
						// *****Listview_1:122** << holds txt objects of Statistics!!! ;)
						// **********Text Objects @Param: Tag{126,127,128,129} ********///
						//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
						var ListView_1 = ScrollView_1.getChildByTag(122);
						cc.log(ListView_1);
						cc.log("listview object above! " + ListView_1);
						//|||||||||||||||||||||||||||||||||||||||||||||||
						// ****Text Objects @Param: Tag{126,127,128,129} ********///
						//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

						////////////////////////////////////////////////////////////////////
						//Todo: Declaration of label Objects derived from css node
						///////////////////////////////////////////////////////////
						//todo: rename - these variables to score and highscore!!!
							//debug 7/24/16:code below works!!
							//this.labelObject = ListView_1.getChildByTag(126);
							//cc.log(this.labelObject);
							//var lo = this.labelObject.getString();
							//var high = this.labelObject.setString("High Score: "
							//                              + this.highscore );
							//cc.log("labelObject is above!" + lo + this.labelObject);
							////cc.log("labelObject >>!" + high + this.labelObject);

							///////////////////////////////////////////////////
							//todo: ## Score//////////////////////////////////
								//**score**/////////////////////////////////////
								this.labelScore1 = ListView_1.getChildByTag(126);
								cc.log(this.labelScore1);
								cc.log("LabelScore1 is above!* :) ");
								this.labelScore1.setString("reset!");
								cc.log(this.labelScore1.getString());
								//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

								//**highscore**/////////////////////////////////////
								this.labelHighScore = ListView_1.getChildByTag(127);
								cc.log(this.labelHighScore);
								cc.log("labelHighScore is above!* :) ");
								this.labelHighScore.setString("reset!");
								cc.log(this.labelHighScore.getString());
							//////////////////////////////////////////////////////////////////


                            ///////////////////////////////////////////////////////////////
                            //todo: ##Coins*******////////////////////////////////////////
								// ##Coin///////////////////////////////////////
								this.labelCoin1 = ListView_1.getChildByTag(128);
								cc.log(this.labelCoin1);
								cc.log("this.LabelCoin1 object >> above!");
								//|||||||||||||||||||||||||||||||||||||||||||||||

								// ##bestCoin/////////////////////////////////////
								this.labelBestCoin = ListView_1.getChildByTag(129);
								cc.log(this.labelBestCoin);
								cc.log("this.labelBestCoin object >> above!");
								//|||||||||||||||||||||||||||||||||||||||||||||||

								// ##TotalCoin//////////////////////////////////////
								this.labelTotalCoin = ListView_1.getChildByTag(130);
								cc.log(this.labelTotalCoin);
								cc.log("this.labelTotalCoin object >> above!");
							//|||||||||||||||||||||||||||||||||||||||||||||||

							//////////////////////////////////////////////////////////////////
							//todo: ##Distance
								//***Distance***/////////////////////////////////////////
								this.labelDistance1 = ListView_1.getChildByTag(131);
								cc.log(this.labelDistance1);
								cc.log("this.labelDistance1 object >> above!");
								//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
							//todo: bestDistance***/////////////////////////////////////////
								this.labelBestDistance = ListView_1.getChildByTag(132);
								cc.log(this.labelBestDistance);
								cc.log("this.labelBestDistance object >> above!");
							//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
							//todo: derive best distance -> loadingBar//////////
							//this.loadingBar = new ccui.LoadingBar();
                            //this.loadingBar.setName("LoadingBar");
                            //this.loadingBar.loadTexture(res.LoadingBar_png);
                            //this.loadingBar.setPercent(0);
                            //this.loadingBar.x = size.width / 2;
                            //this.loadingBar.y = size.height / 2;
                            //this.addChild(this.loadingBar);
                            // Grab node LoadingBar()
                            //loadingBar.setPercent(this.bestDistanceNum || this.distanceNuma);
							//this.loadingBar = Listview_1.getChildByTag(#134);
						    //cc.log("this.loadingBar" + this.loadingBar);
					        //loadingBar.setPercent(0);
						   //|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
								//***TotalDistance***/////////////////////////////////////////
								this.labelTotalDistance = ListView_1.getChildByTag(133);
								cc.log(this.labelTotalDistance);
								cc.log("this.labelTotalDistance object >> above!");
							//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

							//////////////////////////////////////////////////////////////////
							//todo: ##Level ////////////////////////////////////////////////
								//***Level***/////////////////////////////////////////
								this.labelLevel = ListView_1.getChildByTag(134);
								cc.log(this.labelLevel);
								cc.log("this.labelLevel object >> above!");
							//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
							//todo ******** lives remaining
								this.labelLivesRemaining = ListView_1.getChildByTag(135);
								cc.log(this.labelLivesRemaining);
							//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
							//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
								this.labelTotalDeaths = ListView_1.getChildByTag(136);
								cc.log(this.labelTotalDeaths);
							//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
							//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
								this.labelTotalStars = ListView_1.getChildByTag(137);
								cc.log(this.labelTotalStars);
							//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
						/////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				////////////////////////////////////////////
				//*** Declare Panel_Labels && Panel_btn
				///////////////////////////////////////////
				//**Access Point: GameStats.Panel_
				//@Param:Panel_Stats:293
				//@Param:Panel_btn:297
				var Panel_Stats = GameStats.getChildByTag(293);
				cc.log("Panel_Stats" + Panel_Stats);
				//|||||||||||||||||||||||||||||||||||
				var Panel_btn = GameStats.getChildByTag(297);
				cc.log("Panel_btn" + Panel_btn);
				//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

					///////////////////////////////////////
					//** Define Panel_Label Objects***
					//////////////////////////////////////
					//@Param: this.labelCoin:294
					//@Param: this.labelDistance:295
					//@Param: this.labelScore:295

					this.labelCoin = Panel_Stats.getChildByTag(294);
					cc.log("this.labelCoin" + this.labelCoin);

					this.labelDistance = Panel_Stats.getChildByTag(295);
					cc.log("this.labelCoin" + this.labelDistance);

					this.labelScore = Panel_Stats.getChildByTag(296);
					cc.log("this.labelScore" + this.labelScore);

					this.labelKill = Panel_Stats.getChildByTag(297);
					cc.log("this.labelKill" + this.labelKill);
					//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

					///////////////////////////////////////
					//** Define Panel_btn Objects
					//@Param: btn_Resume:298
					//@Param: btn_restart:299
					//@Param: btn_home:300
					//|||||||||||||||||||||||||||||||
					var btn_resume = Panel_btn.getChildByTag(298);
					cc.log("btn_Resume" + btn_resume);
					var btn_restart = Panel_btn.getChildByTag(299);
					cc.log("btn_restart" + btn_restart);
					var btn_home = Panel_btn.getChildByTag(300);
					cc.log("btn_home" + btn_home);
					//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

				//////////////////////////////////////////////////
				//*** Declaration of Touch Event For buttons ***//
				//////////////////////////////////////////////////
	            btn_resume.addTouchEventListener(this.btn_resumeEvent, this);
	            btn_restart.addTouchEventListener(this.btn_restartEvent, this);
	            btn_home.addTouchEventListener(this.btn_homeEvent, this);
				//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

		////////////////////////////////////////////
		//*** todo declare Labels ***
		/////////////////////////////////////////////
		this.score = statistics.coins*10 + statistics.meter*10;
		//||||||||||||||||||||||||||||||||||||||||||||||||
		// ** code for highscore comparison *****/////////
		var tempNum = sys.localStorage.getItem("TempNum");
		if(!isNaN(tempNum)){
			newTempNum = parseInt(tempNum);
			sys.localStorage.setItem("TempNum",newTempNum);
			this.highscore = sys.localStorage.getItem("TempNum");

		} else{
			sys.localStorage.setItem("TempNum",0);
			TempNum = sys.localStorage.getItem("TempNum");
			newTempNum = parseInt(statistics.score);
			sys.localStorage.setItem("TempNum",newTempNum);
			this.highscore = sys.localStorage.getItem("TempNum");
    	    }

		//**todo best score code**//////////////////////////////////
		/*
		## nolan remember best score means the value needs to be
		   reset to null every new instance of the play-scene
		   therefore the bestscore algorithm shall live within
		   the update(dt) function with a local scope comparision
	       value
		*/
		//@param:this.score
		//@param:this.Bsval
		//var bestScoreNum = sys.localStorage.getItem("BestScore");
		//if(!isNaN(scoreNum)){
		//	newScoreNum = parseInt(scoreNum);
		//	sys.localStorage.setItem("BestScore",newScoreNum);
		//	this.bestScoreNum = sys.localStorage.getItem("BestScore");
		//} else{
		//	sys.localStorage.setItem("BestScore",0);
		//	scoreNum = sys.localStorage.getItem("BestScore");
		//	newScoreNum = parseInt(statistics.score);
		//	sys.localStorage.setItem("BestScore",newScoreNum);
		//	this.bestScoreNum = sys.localStorage.getItem("BestScore");
		//    }
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		//////////////////////////////////////////////////////////
		//todo cp coinsnum
		//--totalCoins paradigm then mirror with global values!
        /////////////////////////////////////////////////////
		//** todo Save data to localstorage ****
		//////////////////////////////////////////////////////
		var coinsNum = sys.localStorage.getItem("TotalCoin");
		if(!isNaN(coinsNum)) {
			newCoinsNum = parseInt(coinsNum) + parseInt(statistics.coins);
			sys.localStorage.setItem("TotalCoin",newCoinsNum);
			var a = sys.localStorage.getItem("TotalCoin");
		} else {
			sys.localStorage.setItem("TotalCoin",0);
			coinsNum = sys.localStorage.getItem("TotalCoin");
			newCoinsNum = parseInt(statistics.coins);
			sys.localStorage.setItem("TotalCoin",newCoinsNum);
			var a = sys.localStorage.getItem("TotalCoin");
		}
        /////////////////////////////////////////////////////
		//** todo Write totalDeath Algorithm****
		//////////////////////////////////////////////////////
		var deathNum = sys.localStorage.getItem("TotalDeath");
		if(!isNaN(deathNum)) {
			newDeathNum = parseInt(deathNum) + parseInt(statistics.death);
			sys.localStorage.setItem("TotalDeath",newDeathNum);
			var a = sys.localStorage.getItem("TotalDeath");
		} else {
			sys.localStorage.setItem("TotalDeath",0);
			deathNum = sys.localStorage.getItem("TotalDeath");
			newDeathNum = parseInt(statistics.death);
			sys.localStorage.setItem("TotalDeath",newDeathNum);
			var a = sys.localStorage.getItem("TotalDeath");
		}
		//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
		//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        //** todo Best Coins variable**////////////////////////////
        var coinsNuma = sys.localStorage.getItem("BestCoin");
        if(!isNaN(coinsNuma)) {
			newCoinsNuma = parseInt(coinsNuma);// + parseInt(statistics.meter);
            sys.localStorage.setItem("BestCoin",newCoinsNuma);
            var a = sys.localStorage.getItem("BestCoin");
        } else {
            sys.localStorage.setItem("BestCoin",0);
            distanceNuma = sys.localStorage.getItem("BestCoin");
            newCoinsNuma = parseInt(statistics.coins);
            sys.localStorage.setItem("BestCoin",newCoinsNuma);
            var a = sys.localStorage.getItem("BestCoin");
        }
		//|||||||||||||||||||||||||||||||||||||||||||||||||||||
		//|||||||||||||||||||||||||||||||||||||||||||||||||||||
		//** todo Total Distance variable**///////////////////
		var distanceNum = sys.localStorage.getItem("TotalDistance");
		if(!isNaN(distanceNum)) {
			newDistanceNum = parseInt(distanceNum) + parseInt(statistics.meter);
			sys.localStorage.setItem("TotalDistance",newDistanceNum);
			var a = sys.localStorage.getItem("TotalDistance");
		} else {
			sys.localStorage.setItem("TotalDistance",0);
			distanceNum = sys.localStorage.getItem("TotalDistance");
			newDistanceNum = parseInt(statistics.meter);
			sys.localStorage.setItem("TotalDistance",newDistanceNum);
			var a = sys.localStorage.getItem("TotalDistance");
		}
		//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        //** todo ***Best Distance variable**////////////////////////////
		//**todo step 0 initial step for insertion model!
		// _naming convention place reg Expression 'a' prior variable//
		//best distance should write to the device only if current distance is
		//greater than "BestDistance" value retried!
        //todo this model may need to be cleared once exited to work on second run properly!
        var distanceNuma = sys.localStorage.getItem("BestDistance");
        if(!isNaN(distanceNuma)) {
            //** todo newDistanceNum = -------- + ---------- only when solving for total
            // comment out second parameter (statistics.----) for highscore type problem!
			newDistanceNuma = parseInt(distanceNuma);// + parseInt(statistics.meter);
            sys.localStorage.setItem("BestDistance",newDistanceNuma);
            var a = sys.localStorage.getItem("BestDistance");
        } else {
            sys.localStorage.setItem("BestDistance",0);
            distanceNuma = sys.localStorage.getItem("BestDistance");
            newDistanceNuma = parseInt(statistics.meter);
            sys.localStorage.setItem("BestDistance",newDistanceNuma);
            var a = sys.localStorage.getItem("BestDistance");
        }
		//-----------------------------------------------------------
        /////////////////////////////////////////////////////
		//** todo ** Write Lives Algorithm ****
		//////////////////////////////////////////////////////
		var lifeNum = sys.localStorage.getItem("Lives");
		if(!isNaN(lifeNum)) {
			//newLifeNum = parseInt(lifeNum) - parseInt(statistics.lives);
			newLifeNum = parseInt(lifeNum) - 1;
			sys.localStorage.setItem("Lives",newLifeNum);
			var a = sys.localStorage.getItem("Lives");
		} else {
			sys.localStorage.setItem("Lives",4);
			lifeNum = sys.localStorage.getItem("Lives");
			newLifeNum = parseInt(lifeNum) - 1;
			sys.localStorage.setItem("Lives",newLifeNum);
			var a = sys.localStorage.getItem("Lives");
		}
		//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
		//-----------------------------------------------------------
        /////////////////////////////////////////////////////
		//** TODO: levelNum Algorithm ****
		//Debug: next write this.level in update function
		//////////////////////////////////////////////////////
		var levelNum = sys.localStorage.getItem("Level");
		if(!isNaN(levelNum)) {
			newLevelNum = parseInt(statistics.level) + parseInt(levelNum);
			sys.localStorage.setItem("Level",newLevelNum);//write
			var a = sys.localStorage.getItem("Level");
		} else {
		levelNum = sys.localStorage.getItem("Level");
		newLevelNum = parseInt(statistics.level);//todo: now manipulates the scene//.meter
		//Task:get statistics.level updating wn/play-scene.js
		//Read: Important: By setting a value of statistics.level << play-scene.js, game-over-layer.js this.levelNum
		//(which over writes the storage value "Level" to what ever statistics.level is!!!####
			sys.localStorage.setItem("Level",newLevelNum);
			var a = sys.localStorage.getItem("Level");
		}
		//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        /////////////////////////////////////////////////////
		//** Lives Algorithm ****
		//////////////////////////////////////////////////////
		var lifeNum = sys.localStorage.getItem("Lives");
		if(!isNaN(lifeNum)) {
			//newLifeNum = parseInt(lifeNum) - parseInt(statistics.lives);
			newLifeNum = parseInt(lifeNum) - 1;
			sys.localStorage.setItem("Lives",newLifeNum);
			var a = sys.localStorage.getItem("Lives");
		} else {
			sys.localStorage.setItem("Lives",4);
			lifeNum = sys.localStorage.getItem("Lives");
			newLifeNum = parseInt(lifeNum) - 1;
			sys.localStorage.setItem("Lives",newLifeNum);
			var a = sys.localStorage.getItem("Lives");
		}
		//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        /////////////////////////////////////////////////////
		//** totalDeathNum Algorithm****
		//////////////////////////////////////////////////////
		var deathNum = sys.localStorage.getItem("TotalDeath");
		if(!isNaN(deathNum)) {
			newDeathNum = parseInt(deathNum) + parseInt(statistics.death);
			sys.localStorage.setItem("TotalDeath",newDeathNum);
			var a = sys.localStorage.getItem("TotalDeath");
		} else {
			sys.localStorage.setItem("TotalDeath",0);
			deathNum = sys.localStorage.getItem("TotalDeath");
			newDeathNum = parseInt(statistics.death);
			sys.localStorage.setItem("TotalDeath",newDeathNum);
			var a = sys.localStorage.getItem("TotalDeath");
		}
		//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

		//|||||||||||||||||||||||||||||||||||||||||||||||||||||
		//**Total Stars Collected**///////////////////
		var starsNum = sys.localStorage.getItem("Stars");
		if(!isNaN(starsNum)) {
			newStarsNum = parseInt(starsNum) + parseInt(statistics.stars);
			sys.localStorage.setItem("Stars",newStarsNum);
			var a = sys.localStorage.getItem("Stars");
				//todo|||||||||||||||||||||||||||||||||
				//**Level incremental Code**//
				if(newStarsNum <= 3){
				sys.localStorage.setItem("Level",1);
				}else if(newStarsNum <= 6){
				sys.localStorage.setItem("Level",2);
				}
				//||||||||||||||||||||||||||||||||||
		} else {
			sys.localStorage.setItem("Stars",0);
			starsNum = sys.localStorage.getItem("Stars");
			newStarsNum = parseInt(statistics.stars);
			sys.localStorage.setItem("Stars",newStarsNum);
			var a = sys.localStorage.getItem("Stars");
		}
		//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
		//|||||||||||||||||||||||||||||||||||||||||||||||||||||
		//**TotalBirdsEncountered variable**///////////////////
		//var totalBirdsEnNum = sys.localStorage.getItem("TotalBirdsEn");
		//if(!isNaN(totalBirdsEnNum)) {
		//	newBirdsEnNum = parseInt(totalBirdsEnNum) + parseInt(statistics.coins);
		//	sys.localStorage.setItem("TotalBirdsEn",newBirdsNum);
		//	var a = sys.localStorage.getItem("TotalBirdsEn")d;
		//} else {
		//	sys.localStorage.setItem("TotalBirdsEn",0);
		//	totalBirdsEnNum = sys.localStorage.getItem("TotalBirdsEn");
		//	newBirdsEnNum = parseInt(statistics.birdsEncountered);
		//	sys.localStorage.setItem("TotalBirdsEn",newBirdsEnNum);
		//	var a = sys.localStorage.getItem("TotalBirdsEn");
		//}
        /////////////////////////////////////////////////////////////////////
		//todo you first need to increment stars base on goals
        ///////////////////////////////////////////////////////////////////////
         //Load Global Variables to write to system <-NEEDS to happen before
         //declaration of btn_buymagnet ..... & others!
         //Defined within resource.js
         //@Variable: totalCoin : "TotalCoin"
         //@Variable: magnetNum : "magnet"
         //@Variable: shoesNum : "shoes"
         //@Variable: redshoesNum : " redshoes"
          //todo update these variables below!
			//model todo step #1
		//Global Declaration///////////////////////////////////////////////////////////////////////
          this.totalCoin = sys.localStorage.getItem("TotalCoin");
		  this.bestCoinNum = sys.localStorage.getItem("BestCoin");//most coins/per game
          //|||||||||||||||||||||||||||||||||||||||||||||||||
          this.magnetNum = sys.localStorage.getItem("magnet");
          this.shoesNum = sys.localStorage.getItem("shoes");
          this.redshoesNum = sys.localStorage.getItem("redshoes");
          //||||||||||||||||||||||||||||||||||||||||||||||||||||\
          this.bestDistanceNum = sys.localStorage.getItem("BestDistance");
		  this.totalDistanceNum = sys.localStorage.getItem("TotalDistance");
		  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
          this.lifeNum = sys.localStorage.getItem("Lives");
          this.totalDeathNum = sys.localStorage.getItem("TotalDeath");
          //|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
          this.highScore = sys.localStorage.getItem("HighScore");
   		  this.levelNum = sys.localStorage.getItem("Level");
		  this.stars = sys.localStorage.getItem("Stars");

		  //|||||||||||||||||||||||||||||||||||||||||||||||||||||||
		  this.goalMeter10 = sys.localStorage.getItem("goalMeter10");//boolean 0/1 stored value
          //this.totalJumpNum = sys.localStorage.getItem("TotalJump");
          //this.totalBirdsEncountered = sys.localStorage.getItem("TotalBirdsEn");
          //this.totalBirdsKilled = sys.localStorage.getItem("TotalBirdsKilled");
          //this.bestScore = sys.localStorage.getItem("bestScore");<--bestscore === highscore;
		///////////////////////////////////////////////////////////////////////////////////////////
        this.scheduleUpdate();
	},
	btn_testEvent: function(sender, type)    {
              switch (type)
              {
              case ccui.Widget.TOUCH_BEGAN:
                  cc.log("btn_testEvent Started!");
                  break;
              case ccui.Widget.TOUCH_MOVED:
                  break;
              case ccui.Widget.TOUCH_ENDED:
                  cc.log("btn_test touch ended!")
                  //var scene = new PlayScene();
                  //cc.director.pushScene(scene);
                  break;
              case ccui.Widget.TOUCH_CANCELLED:
                  break;
              }
          },
	createTestMenu:function() {
        var initGOS = function() {
                var PageView_1 = this.GameOverStats.getChildByTag(107);
                cc.log("pageview << function" + PageView_1);
				//@param: 109
				//@param:117 = btn
                 var panel_2 = PageView_1.getChildByTag(109);
   				 cc.log("panel_2 << function!");
				 var btn = panel_2.getChildByTag(117);
				 cc.log(btn);
  }
    return initGOS();
	},
	onData: function(statistics) {
		this.statistics = statistics;
		var roomProperties = Object.keys(this.statistics);
		for (var key of roomProperties) {
		var value = this.statistics[key];
		var log = cc.log("key: " + key + ", value: " + value);
		}
	return log;
	},
	gameoverlayer: function(){
//	createTestMenu: function() {
//	//var size = cc.Director.getInstance().getWinSize();
//	var initGOS = function() {
//	this.gameoverlayer.node = gameoverlayer.node;
//	////////////////////////////////////////////////////////////
//    //**todo GameOverStats.json object **//
//    ///////////////////////////////////////////////////////////
//    //PageViewLayer{Lends from @GameOverLayer.json//
//    //@Param Access Point:FileNode_1:127 //ObjectData : Tag 93
//    		var GameOverStats = this.gameoverlayer.node.getChildByTag(127);
//    		cc.log("Loading GameOverViewLayer below!");
//    		cc.log(GameOverStats);
//    //            /////////////////////////////////////////////
//    //            //Declare Scope of PageView_1 to access buttons
//    //            //@param:PageView_1:
//                var PageView_1 = GameOverStats.getChildByTag(107);
//    //            cc.log(PageView_1);
//    //                    ///////////////////////////////////////
//    //                    //Declaration of Panels within PageView_1
//    //                    //@Param:Panel_2:109
//    //                    //@Param:Panel_3:124
//    //                    //@Param:Panel_15:157
//                        var Panel_1 = PageView_1.getChildByTag(109);
//                        cc.log(Panel_1);
//    //                    ////////////////////////////////////
//    //                    //Within Scope PageView_1 Declare Panels
//    //                    //use tag bc name rending this deep is
//    //                    //wrong on web it says the name
//    //                    //for btn_highscore = button_1 ?
//    //                    //@Param:btn_highscore:117
//    //                    //var btn_highscore = Panel_2.getChildByTag(117);
//    //                    //cc.log(btn_highscore);
//    //                    //todo create wormhole for panels within PageView!!
//    //                    ///////////////////////////////////////////////////////
//    //                    //**Panel_4 : 256  --Holds Panel_Sound
//    //                    //@Param: Panel_Sound:316 --contains sound objects
//    //                    var Panel_4 = PageView_1.getChildByTag(254);
//    //                    cc.log("Panel_4"  + Panel_4);
//    //                    //|||||||||||||||||||||||||||||||||||||||||||||||||||
//    //                        var Panel_Sound = Panel_4.getChildByTag(316);
//    //                        cc.log("Panel_Sound!"  + Panel_Sound);
//    //                        cc.log(Panel_Sound);
//    //                        //** Declare Slider_sound:320
//    //                        var Slider_sound = Panel_Sound.getChildByTag(320);
//    //                        cc.log(Slider_sound);
//    //                       //||||||||||||||||||||||||||||||||||||||||||||||||
//    //                       //** Add Listener to Slider**//
//    //                       Slider_sound.addEventListener(this.sliderEvent,this);
//	}
//
//	initGOS();
//
//	},
//
},
	update: function(dt) {
		var statistics = this.statistics;
		var labelObject = this.labelObject;
		var loadingBar = this.loadingBar;
		/////////////////////////////////////////////////////
		//TODO update(dt) Goals
		///////////////////////////////////////////////
		//this.bestDistanceNum || this.bestDistanceNuma <-local storage api
        if(this.countMeter < this.bestDistanceNum ){
	         this.countMeter++; //single layer scope
		        if (this.countMeter > 1000)
		        {
		            this.countMeter = 0;
		        }

		    loadingBar.setPercent(this.countMeter/100);
        }
		if(this.cval < statistics.coins){
			this.cval++;
			this.Bcval = this.cval;
		}
		//todo compare death to this layer
		if(this.dval < statistics.death) {
			this.dval++;
			this.deathNum = this.dval;
			//todo: insert fn: that passes (this.deathNum)
			//sys.localStorage("totalDeath", someVar);//
		}
		if(this.kval < statistics.kill) {
			this.kval++;
//			statistics.bird = 94;
		}
		if(this.mval < statistics.meter) {
			this.mval++;
			//todo step #2: create header for Bmval
				this.Bmval = this.mval;
		}
		if(this.sval < statistics.score) {
			if(this.score > 1500) {
				this.sval += 20;
				this.Bsval = this.sval;
			} else {
				this.sval += 10;
				this.Bsval = this.sval;
			}
		}
		if(this.leVal < statistics.level) {
			this.leVal++;
			if(this.score > 25){
			//this.stars++;
			//statistics.stars++;
			}
		}
		if (this.starsVal < statistics.stars){
			this.starsVal++;
		//dont need to below code due to getter in playscene.js
		//if(statistics.stars >=3){
		//	this.levelNum = 3;
		//	}
		}
        ////////////////////////////////////////
        //todo Build temporary Level Manager!
        ////////////////////////////////////////
        // Design Ideas -- 1. Moon Warriors -- Level Manager.js
		//** Best Distance Comparison Code**/// todo--step #3
		//** Important to note that you need 2nd neighbor value
		//this.Bmval bc this.mval returns error!
		var _CurrentMeter = parseInt(sys.localStorage.getItem("BestDistance"));
		if (_CurrentMeter < this.Bmval){
			this.Bmval++;
			sys.localStorage.setItem("BestDistance",this.Bmval);

			this.bestDistanceNum = sys.localStorage.getItem("BestDistance");
		}
        /////////////////////////////////////////////////////////////////////////////
		//*** todo BEST COIN ***/////////////
        var _CurrentCoins = sys.localStorage.getItem("BestCoin");
        if (_CurrentCoins < this.Bcval){
            sys.localStorage.setItem("BestCoin",this.Bcval);
            this.bestCoinNum = sys.localStorage.getItem("BestCoin");
        }
        //////////////////////////////////////////////////////
		//////////////////////////////////////////////////////
		//**todo rebuild highscore to grab "highscore" device value
		// instead of creating a new variable!
		var _CurrentScore = sys.localStorage.getItem("TempNum");
		if (_CurrentScore < this.Bsval){
			sys.localStorage.setItem("TempNum",this.Bsval);
			this.highscore = sys.localStorage.getItem("TempNum");
		}
		////////////////////////////////////////////////////////////////
    	//todo: works 9/10
		//        if (isNaN(goalMeter10)) {
		//	        cc.log("goalMeter10 enabled in game-over-layer ctor:!!");
		//            if(statistics.meter >= 5){
		//                statistics.stars++;
		//                cc.log("meter > 10: this.stars++ ");
		//                goalMeter10 = 1;//turn off switch
		//                sys.localStorage.setItem("goalMeter10", 0);//set item to 0/
		//            }
		//		}

		if(this.openStats) {/*
				* openStats - contains important current information to the user
				* of the last run
				**/
			/////////////////////////("Debug:" + this.debugValue);
//			this.labelCoin.setString("..Coins: " + this.cval);
//			this.labelDistance.setString("Distance: " + this.mval);//this.bdVal
//			this.labelScore.setString("TD: " + this.totalDistanceNum);//this.sval
//			this.labelScore.setString("BD: " + this.bestDistanceNum);//'a'@end
//			this.labelKill.setString("TD: " + this.totalDistanceNum);//kval
////			this.labelKill.setString("HiScore: " + this.highscore);//kval
//// *** Debugging BestCoin Variable
               // var status = this.getChildByTag(TagOfGlobal.Death);
////-----------------------------------------------------------------------------
//			this.labelCoin.setString("GD: "+ statistics.death);
//			this.labelDistance.setString("D:" + this.totalDeathNum);
//			this.labelScore.setString("KV*: " + this.kval);
//			this.labelKill.setString("DN:" + this.deathNum);
			//--------------------------------------------------------------

////////////////////////////////////////////////////////////////////////
//// *** todo Debugging lives Variable +++++++++++++++++++++++++++++++++++
////-----------------------------------------------------------------------------
//			this.labelScore.setString("KV*: " + newClock);

//			this.labelCoin.setString("sL: "+ statistics.level);//#@play-scene(_initsta
//			this.labelDistance.setString("lNum:" +this.levelNum);
											//statistics.level
			this.labelCoin.setString("ls.GM10: "+ statistics.level);//#@play-scene(
			this.labelDistance.setString("LN: " + this.levelNum);
			this.labelScore.setString("tStars:" + this.stars);
			this.labelKill.setString("s.stars:" + statistics.stars);
			//this.num = cc.isNumber(5);
			//cc.log("cc.isNumber" + this.num);
			//    cc.log(this.check);
			//    cc.extend(target);
			////cc.each(statistics,function ({cc..log(obj)}));
////------------------------------------------------------------------
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		}
		if(this.openData){/*
                          				* openData - contains in depth detail information to the user
                          				* Current . Best/ high/ total . extra data
                                        * With in Panel #2 -- Stats
                          				**/
			//todo set labels to hold global values///////////////
			////////////////////////////////////////////////
			//this.labelTotalBirdsEncountered.setString(this.totalBirdsEncountered);
			//this.labelTotalBirdsKilled.setString(this.totalBirdsKilled);
		// Modules should contain [sys.localstorage of 1.bestValue 2.TotalValue]
		// Modules include [score,meter,coins,level, deaths, ###jumps, ###kills, ##birds]
	  ////////////////////////////////////////////////////////////////////////////////////////////
		// ***** Todo: Test Module*****//////////////////////////////////////////
		//this.labelObject.setString("Level" + this.levelNum);
		//this.LabelDistance1.setString(this.mval);
		//this.LabelCoin1.setString(this.cval);
		//this.LabelScore1.setString(this.score);
		//cc.log("updated LabelScore! >>" +" "+ this.LabelScore1.getString());

//		this.labelScore1.setString("Score: " + this.score);
//		this.labelHighScore.setString("HighScore: " + this.mval);
		this.labelCoin1.setString("Coins received: " + this.cval);
//		this.labelBestCoin.setString("Most Coins received in a single run: " + this.bestCoinNum);
		this.labelBestCoin.setString("BestDis " + this.bestDistanceNum);
		this.labelLevel.setString("Level: " + this.levelNum);//currentValue	/////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//todo: ##Goal openData panel_Page1!
////////Todo: task: create the label objects in ccs to coorespond these values!
//create goal ideas##
//meter
//score
//coins
//powerup icon
//jumps
//deaths
//birds killed
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
//todo: ##Statistics openData panel_Page!
////////Todo: task; create the label objects in ccs to coorespond these values!
//		//////////////////////////////////////////////////////////
//		//Module: Score /////////
//		/////////////////////////
		this.labelScore1.setString("score:" + this.score);//currentValue
		this.labelHighScore.setString("hscore:" + this.highscore);//bestValue
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

//		////////////////////////////////////////////////////
//		//Module: Coins /////////
//		/////////////////////////
		this.labelCoin1.setString("Coins:" + this.cval);//currentValue
		this.labelBestCoin.setString("BCPR*:" + this.bestCoinNum);// bestValue
		this.labelTotalCoin.setString("tCoin:" + this.totalCoin);//totalValue
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

		//////////////////////////////////////////////////////////////////
		//Module: Meter /////////
		/////////////////////////
		this.labelDistance1.setString("Meter:" + this.mval);//currentValue
		this.labelBestDistance.setString("BDis:" + this.bestDistanceNum);//bestValue
		this.labelTotalDistance.setString("TDis:" + this.totalDistanceNum);//totalValue
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

		//////////////////////////////////////////////////////////////////
		//Module: Level /////////
		/////////////////////////
		this.labelLevel.setString("Level: " + this.levelNum);//stored Value
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

		//////////////////////////////////////////////////////////////////
//TODO		//Module: Deaths /////////
//todo write out algorithm for storage || assign tag #
//		/////////////////////////
		this.labelLivesRemaining.setString("RLives:" + this.lifeNum);//currentValue || remaining
		this.labelTotalDeaths.setString("TDeaths:" + this.totalDeathNum);//totalValue
//		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

//		//////////////////////////////////////////////////////////////////
//TODO		//Module: Jumps /////////
//todo set up algorithm
//		//////////////////////////////////////////////////////////
//		this.labelJumps.setString("Coins: " + this.jval);//currentValue
//		this.labelBestJumps.setString("most jumps per game: " + this.bestJumpNum);//bestValue
//		this.labelTotalJumps.setString("Total Jumps: " + this.totalJumpNum);//totalValue
//		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

//		//////////////////////////////////////////////////////////////////
//TODO		//Module: Icons /////////
//		//////////////////////////////////////////////////////////
//		this.labelCoins.setString(this.totalCoin);
//		this.labelMagnet.setString(this.magnetNum);
//		this.labelShoes.setString(this.shoesNum);
//		this.labelRedshoes.setString(this.redshoesNum);
//		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

		}
		if(this.openStore) {
			this.labelCoins.setString(this.totalCoin);
			this.labelMagnet.setString(this.magnetNum);
			this.labelShoes.setString(this.shoesNum);
			this.labelRedshoes.setString(this.redshoesNum);

		}
	},
	onStore: function() {
		this.openStore = true;
		cc.audioEngine.stopMusic();
		this.totalCoin = sys.localStorage.getItem("TotalCoin");
		this.magnetNum = sys.localStorage.getItem("magnet");
		this.shoesNum = sys.localStorage.getItem("shoes");
		this.redshoesNum = sys.localStorage.getItem("redshoes");

		if(canMusicPlaying) {
//			cc.audioEngine.playMusic(res.sound.shopping, true);
		}
		var winsize = cc.director.getWinSize();
		this.draw = new cc.DrawNode();
		this.draw.drawRect(cc.p(0, winsize.height), cc.p(winsize.width, 0), cc.color(0, 0, 0, 80), 0, cc.color(0, 0, 0, 80));
		this.addChild(this.draw, 4, 1);

		cc.eventManager.addListener({
			event: cc.EventListener.TOUCH_ONE_BY_ONE,
			swallowTouches: true,
			onTouchBegan: function(){return true;},
		}, this.draw);

		this.sboard = new cc.Sprite(res.ui.storeBoard);
		this.sboard.setPosition(cc.p(winsize.width/2+300, winsize.height/2));
		this.sboard.setScale(0.57);
		this.addChild(this.sboard, 5);
		var actionTo = cc.moveTo(1, cc.p(winsize.width/2, winsize.height/2)).easing(cc.easeElasticOut());
		this.sboard.runAction(actionTo);

		this.backBtn = new cc.Menu(new cc.MenuItemSprite(
				new cc.Sprite(res.ui.backBtn),
				new cc.Sprite(res.ui.backBtn),
				this.backToMenu, this));
		this.backBtn.setPosition(cc.p(winsize.width+100, 60));
		this.backBtn.attr({
			anchorX: 0,
			anchorY: 0,
			x: winsize.width/2+300,
			y: winsize.height/2-190
		});
		this.backBtn.setScale(0.6);
		this.backBtn.runAction(cc.moveTo(1, cc.p(winsize.width/2-100, winsize.height/2-210)).easing(cc.easeElasticOut()));
		this.addChild(this.backBtn, 6);

		//show coins nums
		this.labelCoins = new cc.LabelTTF(this.totalCoin, "Helvetica", 50);
		this.labelCoins.setColor(cc.color(255, 255, 255));//white color
		this.labelCoins.setPosition(cc.p(winsize.width+100, winsize.height/2+128));
		this.labelCoins.setScale(0.3);
		this.addChild(this.labelCoins, 10);
		//this.labelCoins.retain();
		this.labelCoins.runAction(cc.moveTo(1, cc.p(winsize.width/2+50, winsize.height/2+128)).easing(cc.easeElasticOut()));

		this.buyMagnetBtn = new cc.Menu(new cc.MenuItemSprite(
				new cc.Sprite(res.ui.buy30),
				new cc.Sprite(res.ui.buy30),
				function(){
					//buy magnet
					if(this.totalCoin - 30 < 0){
						return;
					}
					this.totalCoin -= 30;
					this.magnetNum++;
					sys.localStorage.setItem("TotalCoin", this.totalCoin);
					sys.localStorage.setItem("magnet", this.magnetNum);
//					cc.audioEngine.playEffect(res.sound.button);
				}, this));
		this.buyMagnetBtn.setPosition(cc.p(winsize.width+80, winsize.height/2+70));
		this.buyMagnetBtn.attr({
			anchorX: 0,
			anchorY: 0
		});
		this.buyMagnetBtn.setScale(0.6);
		this.buyMagnetBtn.runAction(cc.moveTo(1, cc.p(winsize.width/2+80, winsize.height/2+70)).easing(cc.easeElasticOut()));
		this.addChild(this.buyMagnetBtn, 6);

		this.buyShoesBtn = new cc.Menu(new cc.MenuItemSprite(
				new cc.Sprite(res.ui.buy50),
				new cc.Sprite(res.ui.buy50),
				function(){
					//buy shoes
					if(this.totalCoin - 50 < 0){
						return;
					}
					this.totalCoin -= 50;
					this.shoesNum++;
					sys.localStorage.setItem("TotalCoin", this.totalCoin);
					sys.localStorage.setItem("shoes", this.shoesNum);
//					cc.audioEngine.playEffect(res.sound.button);
				}, this));
		this.buyShoesBtn.setPosition(cc.p(winsize.width+80, winsize.height/2-10));
		this.buyShoesBtn.attr({
			anchorX: 0,
			anchorY: 0
		});
		this.buyShoesBtn.setScale(0.6);
		this.buyShoesBtn.runAction(cc.moveTo(1, cc.p(winsize.width/2+80, winsize.height/2-10)).easing(cc.easeElasticOut()));
		this.addChild(this.buyShoesBtn, 6);

		this.buyRedshoesBtn = new cc.Menu(new cc.MenuItemSprite(
				new cc.Sprite(res.ui.buy50),
				new cc.Sprite(res.ui.buy50),
				function(){
					//buy red shoes
					if(this.totalCoin - 50 < 0){
						return;
					}
					this.totalCoin -= 50;
					this.redshoesNum++;
					sys.localStorage.setItem("TotalCoin", this.totalCoin);
					sys.localStorage.setItem("redshoes", this.redshoesNum);
//					cc.audioEngine.playEffect(res.sound.button);
				}, this));
		this.buyRedshoesBtn.setPosition(cc.p(winsize.width+80, winsize.height/2-90));
		this.buyRedshoesBtn.attr({
			anchorX: 0,
			anchorY: 0
		});
		this.buyRedshoesBtn.setScale(0.6);
		this.buyRedshoesBtn.runAction(cc.moveTo(1, cc.p(winsize.width/2+80, winsize.height/2-90)).easing(cc.easeElasticOut()));
		this.addChild(this.buyRedshoesBtn, 6);

		/**
		 * show prop nums of own
		 */
		//show magnet
		this.labelMagnet = new cc.LabelTTF(this.magnetNum, "Helvetica", 60);
		this.labelMagnet.setColor(cc.color(255, 255, 255));//white color
		this.labelMagnet.setPosition(cc.p(winsize.width+100, winsize.height/6-5));
		this.labelMagnet.setScale(0.3);
		this.addChild(this.labelMagnet, 10);
		this.labelMagnet.runAction(cc.moveTo(1, cc.p(winsize.width/2-70, winsize.height/6-5)).easing(cc.easeElasticOut()));

		//show shoes
		this.labelShoes = new cc.LabelTTF(this.magnetNum, "Helvetica", 60);
		this.labelShoes.setColor(cc.color(255, 255, 255));//white color
		this.labelShoes.setPosition(cc.p(winsize.width+100, winsize.height/6-5));
		this.labelShoes.setScale(0.3);
		this.addChild(this.labelShoes, 10);
		this.labelShoes.runAction(cc.moveTo(1, cc.p(winsize.width/2+20, winsize.height/6-5)).easing(cc.easeElasticOut()));

		//show redshoes
		this.labelRedshoes = new cc.LabelTTF(this.magnetNum, "Helvetica", 60);
		this.labelRedshoes.setColor(cc.color(255, 255, 255));//white color
		this.labelRedshoes.setPosition(cc.p(winsize.width+100, winsize.height/6-5));
		this.labelRedshoes.setScale(0.3);
		this.addChild(this.labelRedshoes, 10);
		this.labelRedshoes.runAction(cc.moveTo(1, cc.p(winsize.width/2+100, winsize.height/6-5)).easing(cc.easeElasticOut()));
	},
	btn_restartEvent: function(sender, type){
        switch (type)
        {
        case ccui.Widget.TOUCH_BEGAN:
            break;
        case ccui.Widget.TOUCH_MOVED:
            break;
        case ccui.Widget.TOUCH_ENDED:
            cc.log("btn_restartEvent  touch ended!")
            cc.director.runScene(new this.playSceneClass());
            break;
        case ccui.Widget.TOUCH_CANCELLED:
            break;
        }
    },
	btn_resumeEvent: function(sender, type)    {
        switch (type)
        {
        case ccui.Widget.TOUCH_BEGAN:
            break;
        case ccui.Widget.TOUCH_MOVED:
            break;
        case ccui.Widget.TOUCH_ENDED:
            cc.log("btn_resumeEvent touch ended!")
		//todo fix resume Event
		//to fix this resume function is not within statistics layer or the old game-over-layer.j
		//** to fix this problem you which was inherented from alto adventure game
		//** You will need to research to create this task brand new object pausing
		//algorthim will look like
		//  if {Pause PlayScene()}
		//		cc.director.runScene(GameOverLayer())<-- with Pause button enabled
		// -- to make this eaiser to see draw out basic outline for this algorthim above!
		//            var scene = new WelcomeScene();
		//            cc.director.runScene(scene);
		/////////////////////////////////////////////////////////////
		//// example
		//  // CallFunc without data
		//  var finish = new cc.CallFunc(this.removeSprite, this);
		//
		//  // CallFunc with data
		//  var finish = new cc.CallFunc(this.removeFromParentAndCleanup, this,  true);
            break;
        case ccui.Widget.TOUCH_CANCELLED:
            break;
        }
    },
	btn_homeEvent: function(sender, type){
        switch (type){
        case ccui.Widget.TOUCH_BEGAN:
            break;
        case ccui.Widget.TOUCH_MOVED:
            break;
        case ccui.Widget.TOUCH_ENDED:
            cc.log("btn_home  touch ended-->WelcomeScene()")
            //cc.director.purgeCachedData();
            cc.director.pushScene(new WelcomeScene());
            //cc.director.purgeDirector();
            break;
        case ccui.Widget.TOUCH_CANCELLED:
            break;
        }
    },
	onMenu: function() {
//		var action = cc.sequence(
//				cc.moveTo(0.7, cc.p(-300, winSize.height/2)).easing(cc.easeElasticInOut(0.45)));
//		this.board.runAction(action);
//		this.menuBtn.runAction(cc.sequence(
//				cc.moveTo(0.7, cc.p(-300, winSize.height/2-165)).easing(cc.easeElasticInOut(0.45)),
				cc.callFunc(function(){
					cc.director.replaceScene(new WelcomeScene());
				}.bind(this));
		//play button effect
	},
	onUpload: function () {
		var statistics = this.statistics;
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				cc.log(xhr.readyState);
			}
		};
		xhr.open('POST', /*'/**http://endless-journey-server.coding.io/game/scores*/'', true);
		xhr.setRequestHeader('Content-Type', 'application/json');
		var username = sys.localStorage.getItem("username");
		xhr.send(JSON.stringify({
			id: username,
			meter: statistics.meter,
			score: statistics.score,
			coins: statistics.coins
		}));
	},
	onRestart: function (sender) {
		var winSize = cc.director.getWinSize();
//		var actionTo1 = cc.moveTo(0.7, cc.p(winSize.width/2-120, winSize.height/2-200)).easing(cc.easeBounceOut());
		var action = cc.sequence(
				cc.moveTo(0.7, cc.p(-300, winSize.height/2)).easing(cc.easeElasticInOut(0.45)));
		this.board.runAction(action);
		this.restartBtn.runAction(cc.sequence(
				cc.moveTo(0.7, cc.p(-300, winSize.height/2-165)).easing(cc.easeElasticInOut(0.45)),
				cc.callFunc(function(){
				    ///////////////////////////////
				    //stop store music from play with playscene's game music!
				    cc.audioEngine.stopMusic();
				    ///////////////////////////
					cc.director.runScene(new this.playSceneClass());
				}.bind(this))));
		this.storeBtn.runAction(cc.sequence(
				cc.moveTo(0.7, cc.p(-300, winSize.height/2-165)).easing(cc.easeElasticInOut(0.45))));

		this.menuBtn.runAction(cc.sequence(
				cc.moveTo(0.7, cc.p(-300, winSize.height/2-165)).easing(cc.easeElasticInOut(0.45))));
		//play button effect
//		cc.audioEngine.playEffect(res.sound.button);

		//label out
		this.labelCoin.runAction(cc.sequence(
				cc.moveTo(0.7, cc.p(-300, winSize.height/2+25)).easing(cc.easeElasticInOut(0.45))));
		this.labelKill.runAction(cc.sequence(
				cc.moveTo(0.7, cc.p(-300, winSize.height/2+85)).easing(cc.easeElasticInOut(0.45))));
		this.labelDistance.runAction(cc.sequence(
				cc.moveTo(0.7, cc.p(-300, winSize.height/2-35)).easing(cc.easeElasticInOut(0.45))));
		this.labelScore.runAction(cc.sequence(
				cc.moveTo(0.7, cc.p(-300, winSize.height/2-95)).easing(cc.easeElasticInOut(0.45))));
	},
	sliderEvent: function(sender,type) {
//	            switch(type)
//	            {
//	                case ccui.Slider.EVENT_PERCENT_CHANGED:
//	                cc.log("percent" + sender.getPercent().toFixed(0));
//	                var Volume = sender.getPercent();
//	                Volume = Volume/100;
//	                cc.audioEngine.setMusicVolume(Volume);
//	                cc.log(Volume);
//	                break;
//	            }
	        },
	onEnter: function() {
		this._super();
		this.scheduleUpdate();
	},
	onExit: function() {
		this._super();
		this.unscheduleUpdate();
		//todo clear
	}

});
