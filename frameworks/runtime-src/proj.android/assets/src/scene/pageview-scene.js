var PageViewScene = cc.Scene.extend(/** @lends WelcomeScene# */{

    /**
     * Constructor of cc.Scene
     */
    _className: "PageViewScene",

    ctor: function () {
        this._super();
        this.init();
        var layer = new PageViewLayer();
        this.addChild(layer,1);
    }
});

