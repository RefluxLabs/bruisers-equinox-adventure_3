///////////////////////////////////////////////////
/**PageViewLayer({@Extends from PageViewScene(){
*PageViewLayer is a perfect example of coding a cocosstudio scene node
*that has a layer as a dependent node
*This layer shows how to:    1. Load json
	                         2. Debug json
	                         3. Declare Global and Local Widgets
	                         4. Define Global and Local Widgets TouchEventListeners
*/////////////////////////////////////////////////////
var PageViewLayer = cc.Layer.extend({
    sprite:null,
    btn_back:null,
    btn_highscore:null,
    ctor:function () {
        this._super();
        var size = cc.winSize;
        ////////////////////////////////////
        //Initialize and Load Json Scene
        var pageviewscene = ccs.load(res.IAP.PageViewScene_json);
        this.addChild(pageviewscene.node);
        cc.log(pageviewscene);
        /////////////////////////////////////////////////
        //PageViewLayer Debug Module logs data to ensure it works
        //Define PageViewLayer to access the children
         var PageViewLayer = pageviewscene.node.getChildByTag(110);
         cc.log("PageViewLayerBelow!");
         cc.log(PageViewLayer);
//        //need to test this below 4-6-16
//        //var data = PageViewLayer._getPageCount();
//        //cc.log(data);
            ////////////////////////////////////////////
            //Define Global buttons within PageViewLayer
            //@Param:btn_back:tag#(38)
            var btn_back = PageViewLayer.getChildByTag(38);
            cc.log(btn_back);
                /////////////////////////////////////////////
                //Declare Scope of PageView_1 to access buttons
                //@param:PageView_1:
                var PageView_1 = PageViewLayer.getChildByTag(107);
                cc.log(PageView_1);
                    /////////////////////////////////////
                    //Declaration of Panels within PageView_1
                    //@Param:Panel_1:108
                    //@Param:Panel_2:109
                    //@Param:Panel_3:124
                    //@Param:Panel_15:157
                   var Panel_1 = PageView_1.getChildByTag(109);
                   cc.log(Panel_1);
                    //////////////////////////////////
                    //Within Scope PageView_1 Declare Panels
                    //use tag bc name rending this deep is
                    //wrong on web it says the name
                    //for btn_highscore = button_1 ?
                    //@Param:btn_highscore:117
                    //var btn_highscore = Panel_2.getChildByTag(117);
                    //cc.log(btn_highscore);
                    ///////////////////////////////////////////////////////
					//Panel_4 : 256  --Holds Panel_Sound
                    //@Param: Panel_Sound:316 --contains sound objects
                    var Panel_4 = PageView_1.getChildByTag(254);
                    cc.log("Panel_4"  + Panel_4);
                    //|||||||||||||||||||||||||||||||||||||||||||||||||||
                        var Panel_Sound = Panel_4.getChildByTag(316);
                        cc.log("Panel_Sound!"  + Panel_Sound);
                        cc.log(Panel_Sound);
	             	// Declare Slider_sound:320
                    var Slider_sound = Panel_Sound.getChildByTag(320);
                    cc.log(Slider_sound);
                   //||||||||||||||||||||||||||||||||||||||||||||||||

	   //** Add Listener to Slider**/
	   // Slider_sound.addEventListener(this.sliderEvent,this);
       //////////////////////////////////////////////////
       //Declare TouchEventListeners for above params
       //@Global Param:btn_back
       //@Local Param:btn_highscore
       //@Local Param:btn_play
       btn_back.addTouchEventListener(this.btn_backEvent, this);
       //btn_highscore.addTouchEventListener(this.btn_highscoreEvent, this);
    },
        sliderEvent: function(sender,type)
        {   switch(type)
            {
                case ccui.Slider.EVENT_PERCENT_CHANGED:
                cc.log("percent" + sender.getPercent().toFixed(0));
                var Volume = sender.getPercent();
                Volume = Volume/100;
                cc.audioEngine.setMusicVolume(Volume);
                cc.log(Volume);
                break;
            }
        },
       btn_backEvent: function(sender, type)
        {
            switch (type)
            {
            case ccui.Widget.TOUCH_BEGAN:
                break;
            case ccui.Widget.TOUCH_MOVED:
                break;
            case ccui.Widget.TOUCH_ENDED:
                cc.log("btn_back touch ended!")
               cc.director.pushScene(new WelcomeScene());
                break;
            case ccui.Widget.TOUCH_CANCELLED:
                break;
            }
        },

});
