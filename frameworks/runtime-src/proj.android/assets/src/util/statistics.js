var statistics = {
    coins: 0,
    meter: 0,
    kill: 0,
    death: 0,
    highscore: 0,
    lives: 1,
    jumps: 0,
    birdsEncountered: 0,
    birdsKilled: 0,
    previousScore: 0,
    currentScore: 0,
    highScore: 49,
    BestDistance: 0,
    d : 0,
    level : 0,
    statsArr: [],
//    statistics: null,

    get score() {
        return this.coins * 10;
    },

    get length() {
        return parseInt(this.hero.sprite.getPositionX() / 100)
    },

    addDeath: function (d) {
        this.death += d;
        //this.labelCoin.setString("Coins:" + this.coins);
    },

    reset: function (hero) {
        this.hero = hero;
        this.coins = 0;
        this.meter = 0;
        this.kill = 0;
    },
    getName: function(name){
    return "statistics";
    },
    getObj: function(){
    return statistics;
    }



};