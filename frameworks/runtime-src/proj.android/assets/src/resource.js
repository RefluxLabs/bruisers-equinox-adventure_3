
var res = {

  //  HelloWorld_png : "res/HelloWorld.png",
   // MainScene_json : "res/MainScene.json",//this style does not work!!:{
   //   MainScene_json : 'res/MainScene.json',


IAP:{
    button_png : 'res/ui/button.png',
    button_highlight_png : 'res/ui/button_highlight.png',
    hudlayer_json : 'res/hudlayer.json',
    StoreLayer1_json :'res/StoreLayer_0.json',
    GameOverLayer_json : 'res/GameOverLayer.json',
    GameOverStats_json : 'res/GameOverStats.json',
    GameOverView_json : 'res/GameOverView.json',
    MainScene_json : 'res/MainScene.json',
    PageViewScene_json : 'res/PageViewScene.json',
    PageViewLayer_json : 'res/PageViewLayer.json',
    MenuScene_json : 'res/MenuScene.json',
    StoreScene_json : 'res/StoreScene.json',
    StoreLayer_json : 'res/StoreLayer.json',
    StoreLayer_0_json : 'res/StoreLayer_0.json',
    HelloWorld_png : 'res/Environment/HelloWorld.png',
    CloseNormal_png : 'res/ui/CloseNormal.png',
    CloseSelected_png : 'res/ui/CloseSelected.png',
},

    //opening
    open: {
        bg: 'res/Environment/open-bg.png',
        team: 'res/RefluxLabs_Logo.png'
    },
    ///////////////////
    mode: {
        board: 'res/Environment/Boards/mode-board.png',
        mode1: 'res/Environment/Boards/mode1.png',
        mode2: 'res/Environment/Boards/mode2.png'
    },

    info: {
        board: 'res/Environment/Boards/info-board.png',
        done: 'res/ui/done.png'
    },

    rank: {
        board: 'res/Environment/Boards/rank-board.png'
    },

    //////////////////
    //menu
    menu: {
        bg: 'res/Environment/Parallax/menu-bg.png',
        playBtn: 'res/ui/Btn/play-btn.png',
        playBtnS: 'res/ui/Btn/play-btn-s.png',
        storeBtn: 'res/ui/Btn/rank-btn.png',
        storeBtnS: 'res/ui/Btn/rank-btnS.png',
        setBtn: 'res/ui/Btn/set-btn.png',
        setBtnS: 'res/ui/Btn/set-btn-s.png',
        startBtn: 'res/ui/Btn/start-btn-normal.png',
        startBtnS: 'res/ui/Btn/start-btn-selected.png',
        aboutBtn: 'res/ui/Btn/about-btn.png',
        aboutBtnS: 'res/ui/Btn/about-btn-s.png',
        logo: 'res/Environment/game-logo.png',
        logos: 'res/RefluxLabs_Logo.png',
        wait: 'res/Environment/Boards/wait.png',
        enable: 'res/ui/enable.png',
        disable: 'res/ui/disable.png'
    },

    panda: {
        plist: 'res/Roles/panda.plist',
        png: 'res/Roles/panda.png'
    },
    //todo create new bruiser loader like panda
   bruiser: {
           plist:'res/bruiser.plist',
           png:'res/bruiser.png'
   },

    //todo create new platform images in AI
    //fixed: 4 assending ordered platform
    platform: {
        plist: 'res/Objects/platform.plist',
        png: 'res/Objects/platform.png'

    },

    // Background
    background: [
        'res/Environment/Parallax/far-bg.png',
        'res/Environment/Parallax/near-bg.png',
        'res/environment1/BG_1.png',
        'res/environment1/Fog_3.png'
    ],
    // gold
    gold: {
        plist: 'res/Objects/gold.plist',
        png: 'res/Objects/gold.png'
    },

    //enemy
    enemy: {
        png: 'res/Roles/enemy.png',
        plist: 'res/Roles/enemy.plist'
    },

    //bird
    bird: {
        png: 'res/Roles/bird.png',
        plist: 'res/Roles/bird.plist'
    },
    //Created Icons plist which holds magnet.png
    //@shoes.png
    //@redshoes.png
    //magnet todo insert better magnet.plist & png
    magnet: {
        png: 'res/Objects/Icons.png',
        plist: 'res/Objects/Icons.plist',//magnet.plist
        effect: 'res/Objects/magnetEffect.png'
    },

    //spring
    spring: {
        png: 'res/Objects/spring.png',
        plist: 'res/Objects/spring.plist'
    },

    //todo insert better shoes!
    shoes: {
        png: 'res/Objects/Icons.png',
        plist: 'res/Objects/Icons.plist'
    },

    //red shoes todo create better plist and png!
    redshoes: {
        png: 'res/Objects/Icons.png',
        plist: 'res/Objects/Icons.plist'
    },

    //particle
    particle: {
        circle: 'res/Objects/circle_particle.plist',
        stars: 'res/Objects/stars_particle.plist'
    },

    //fire
    fire: {
        plist: 'res/Objects/fire.plist'
    },

    //game over res
    over: {
        board: 'res/ui/score-board.png',
        store: 'res/ui/store.png',
        reload: 'res/ui/reload.png',
        menu: 'res/ui/menu.png'
    },

    // Sound Effect
    sound: {
        bg_mp3: 'res/sound/bg.mp3',
        jump_mp3: 'res/sound/jump.mp3',
        gold_mp3: 'res/sound/eat_gold.mp3',
        game_over: 'res/sound/game_over.mp3',
        button: 'res/sound/button.mp3',
        menu: 'res/sound/menu.mp3',
        opening: 'res/sound/opening.mp3',
        enemyDied: 'res/sound/enemyDied.mp3',
        magnet: 'res/sound/magnet.mp3',
        lose_prop: 'res/sound/lose_prop.mp3',
        spring: 'res/sound/spring.mp3',
        speedup: 'res/sound/speedup.mp3',
        alert: 'res/sound/alert.mp3',
        shopping: 'res/sound/shopping.mp3'
    },

    ui: {
        goldbar: 'res/ui/Hud/gold-bar.png',
        energybar: 'res/ui/Hud/energy-bar.png',
        progress: 'res/ui/Hud/progress.png',
        soundOn: 'res/ui/soundOnBtn.png',
        soundOff: 'res/ui/soundOffBtn.png',
        distance: 'res/ui/Hud/distance.png',
        aboutBoard: 'res/ui/about-board.png',
        backBtn: 'res/ui/back-btn.png',
        setBoard: 'res/ui/set-board.png',
        onBtn: 'res/ui/on-btn.png',
        offBtn: 'res/ui/off-btn.png',
        highBtn: 'res/ui/high-btn.png',
        lowBtn: 'res/ui/low-btn.png',
        storeBoard: 'res/ui/store-board.png',
        buy30: 'res/ui/buy-30.png',
        buy50: 'res/ui/buy-50.png',
        magnetProp: 'res/ui/Hud/magnet-prop.png',
        shoesProp: 'res/ui/shoes-prop.png',
        redshoesProp: 'res/ui/Hud/redshoes-prop.png'
    },
    physics: {
        groundHeight: -1000
    }
};

if(typeof SpriteTag == "undefined") {

    var SpriteTag = {
        player: 0,
        gold: 1,
        inventory: 2,
        platform: 3,
        ground: 4,
        magnet: 5,
        spring: 6,
        shoes: 7,
        redshoes: 8,
        bird: 9
    };

};

if(typeof TagOfGlobal == "undefined") {
    var TagOfGlobal = {};
    TagOfGlobal.Death = 0;
    TagOfGlobal.Animation = 1;
    TagOfGlobal.Status = 2;
};

var g_resources = [];
for (var i in res) {
    for (var j in res[i]) {
        g_resources.push(res[i][j]);
    }
}


//game global variable
var gameStarted = false;
var firstInit = true;

//load game audio sys
var canMusicPlaying = 0;
var canAudioPlaying = 0;
var diffDeg = 0;
var isMusicPlaying = 0;
var sys = cc.sys;

var _goalMeter10 = 0;
var goalMeter20 = 0;
var goalMeter30 = 0;
//////////////////////////////////////////
// BE_ Goal << State mechanism for goals
var BE_Goal = {
    stage0: true,
    stage1: false,
    stage2: false,
    stage3: false
};


//////////////////////////////
var localStorage = sys ? sys.localStorage : window.localStorage;
if (!localStorage.getItem("canAudioPlaying") || !localStorage.getItem("canMusicPlaying")) {
    localStorage.setItem("canMusicPlaying", 1);
    localStorage.setItem("canAudioPlaying", 1);
    localStorage.setItem("diffDeg", 0);
}
canMusicPlaying = parseInt(localStorage.getItem("canMusicPlaying"));
canAudioPlaying = parseInt(localStorage.getItem("canAudioPlaying"));
diffDeg = parseInt(sys.localStorage.getItem("diffDeg"));

//////////////////////////////////////////////////////////////////
//// BE_ Goal << State mechanism for goals
//Todo ** Read IBM article on OOP Game Design State Mechanism!!
//////////////////////////////////////////////////////////////////
//initialize prop to local
if (!localStorage.getItem("magnet") || !localStorage.getItem("shoes") || !localStorage.getItem("redshoes")) {
    localStorage.setItem("magnet", 0);
    localStorage.setItem("shoes", 0);
    localStorage.setItem("redshoes", 0);
}

/////////////////////////////////////////////
////**initialize Stats to local**////
////////////////////////////////////////////
if ( !localStorage.getItem("Level") ||
     !localStorage.getItem("Lives") ||
     !localStorage.getItem("TotalDeath") ||
     !localStorage.getItem("TotalJump") ||
     !localStorage.getItem("TotalBirdsEn") ||
     !localStorage.getItem("TotalBirdsKilled") ||
     !localStorage.getItem("TotalDistance") ||
     !localStorage.getItem("BestDistance") ||
     !localStorage.getItem("HighScore") ||
     !localStorage.getItem("BestScoreNum") ||
     !localStorage.getItem("BestCoinNum") ||
     !localStorage.getItem("Stars") ||
     !localStorage.getItem("goalMeter10") ||
     !localStorage.getItem("goalMeter20") ||
     !localStorage.getItem("goalMeter30")

     //!localStorage.getItem("Template")
    ){
    localStorage.setItem("Stars", 0);
    localStorage.setItem("goalMeter10",0);
    localStorage.setItem("goalMeter20",0);
    localStorage.setItem("goalMeter30",0);
    localStorage.setItem("Level", 0);
    localStorage.setItem("Lives", 4);
    localStorage.setItem("TotalDeath", 0);
    localStorage.setItem("TotalJump", 94);
    localStorage.setItem("TotalBirdsEn", 94);
    localStorage.setItem("TotalBirdsKilled", 94);
    localStorage.setItem("TotalDistance", 0);
    localStorage.setItem("BestDistance",0);
    localStorage.setItem("HighScore", 94);
    localStorage.setItem("BestScore",94);
    localStorage.setItem("BestCoin",0);
}
//todo load Global variables!
goalMeter10 = parseInt(localStorage.getItem("goalMeter10"));
goalMeter20 = parseInt(localStorage.getItem("goalMeter20"));
goalMeter30 = parseInt(localStorage.getItem("goalMeter30"));


_goalMeter20 = parseInt(localStorage.getItem("goalMeter20"));
_goalMeter30 = parseInt(localStorage.getItem("goalMeter30"));
_Stars = parseInt(localStorage.getItem("Stars"));
_Level = parseInt(localStorage.getItem("Level"));
_Lives = parseInt(localStorage.getItem("Lives"));
_TotalDeath = parseInt(sys.localStorage.getItem("TotalDeath"));
_TotalBirdsEn = parseInt(localStorage.getItem("TotalBirdsEn"));
_TotalBirdsKilled = parseInt(localStorage.getItem("TotalBirdsKilled"));
_TotalDistance = parseInt(localStorage.getItem("TotalDistance"));
_BestDistance = parseInt(localStorage.getItem("BestDistance"));
_HighScore = parseInt(localStorage.getItem("HighScore"));
_BestScoreNum = parseInt(localStorage.getItem("BestScoreNum"));
_BestCoinNum = parseInt(localStorage.getItem("BestCoinNum"));

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//preload objects defined
var pre_bird, pre_frog, pre_magnet, pre_redshoes, pre_shoes, pre_spring;
var new_space = [];